<?php

function noblogs_split_db($file) {
    $db_url = trim(file_get_contents($file));
    return parse_url($db_url);
}

function noblogs_load_backends($db_config_file, $hashptr) {
  global $wpdb;

  $backend_map = array();

  $fp = @fopen($db_config_file, "r");
  if (!$fp) {
    die("Database backends not configured!");
  }
  while (($line = fgets($fp, 1024)) !== false) {
    $wline = rtrim($line);
    if ($wline == "" || $wline[0] == '#') {
      continue;
    }
    $line_parts = explode(" ", $wline);
    $server_id = $line_parts[0];
    $dataset = "backend_" . $server_id;
    $backend_url = $line_parts[2];
    $backend_url_data = parse_url($backend_url);
    $backend = array(
        "host" => $backend_url_data["host"] . ":" . $backend_url_data["port"],
        "user" => $backend_url_data["user"],
        "password" => $backend_url_data["pass"],
        "name" => substr($backend_url_data["path"], 1),
        "dataset" => $dataset,
        "read" => 1, "write" => 1, "timeout" => 10
        );
    $wpdb->add_database($backend);
    $hashptr->addTarget($dataset);
    $backend_map[$dataset] = $backend;
  }
  fclose($fp);
  return $backend_map;
}

function noblogs_load_global_dataset($master_file, $ip_file) {
    global $wpdb;
    $mdata = noblogs_split_db($master_file);
    $ldata = trim(file_get_contents($ip_file));
    $globaldb = array(
        "host" => $mdata["host"] . ":" . $mdata["port"],
        "user" => $mdata["user"],
        "password" => $mdata["pass"],
        "name" => substr($mdata["path"], 1),
        "dataset" => "global",
        "read" => 1, "write" => 1, "timeout" => 2
    );
    if ($mdata['host'] == $ldata) {
        $wpdb->add_database($globaldb);
    } else {
        $globaldb['read'] = 0;
        $wpdb->add_database($globaldb);
        $globaldb['host'] = $ldata . ":" . $mdata["port"];
        $globaldb['read'] = 1;
        $globaldb['write'] = 0;
        $wpdb->add_database($globaldb);
    }
}
