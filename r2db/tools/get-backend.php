#!/opt/noblogs/cron/php-noblogs
<?php

if ($argv[1]) {
  $blog_domain = $argv[1];
  if (!preg_match('/\.noblogs\.org$/', $blog_domain)) {
    $blog_domain = $blog_domain . '.noblogs.org';
  }
} else {
  die("Usage: get-backend <blog_name>\n");
}

// Load wordpress api.
define('WP_CACHE',false);
/** Setup WordPress environment */
require_once('wp-load.php');
require_once('db-config.php');


function new_hash($dbid, $reversemap) {
  global $wpdb;
  $lookup = $wpdb->hash_map->lookup($dbid);
  $backend = $reversemap[$lookup];
  $result = array();
  if (preg_match('/^(.*):([0-9]*)$/', $backend['host'], $matches)) {
    $result['host'] = $matches[1];
    $result['port'] = $matches[2];
  }
  $result['user'] = $backend['user'];
  $result['password'] = $backend['password'];
  $result['db'] = $backend['name'];
  return $result;
}

function get_blog($domain) 
{
  global $wpdb;

//  $sql = "SELECT blog_id, domain, path FROM $wpdb->blogs WHERE public = 1 AND deleted = 0 AND archived = '0' ORDER BY domain, path";
  $sql = "SELECT blog_id, domain, path FROM $wpdb->blogs WHERE domain = '" . $domain . "'";
  $result = $wpdb->get_results($sql);
  return ($result[0]);
}

$hashmap = new Flexihash();
$reverse_backend_map = noblogs_load_backends(NOBLOGS_BACKEND_CONFIG, $hashmap);

$blog = get_blog($blog_domain);
$backend = new_hash($blog->blog_id, $reverse_backend_map);

echo "\n" . $blog_domain . " (id " . $blog->blog_id . ")\n\n";
print_r($backend);

