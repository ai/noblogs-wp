#!/opt/noblogs/cron/php-noblogs
<?php

// Load wordpress api.
define('WP_CACHE',false);
/** Setup WordPress environment */
require_once('wp-load.php');
require_once('db-config.php');


function old_hash($dbid) {
  if (($dbid % 2) == 0) {
    return array('host' => '172.16.1.3',
		 'port' => '3307',
		 'user' => 'noblogs',
		 'password' => 'n0bl0gst3st',
		 'db' => 'noblogs_2');
  } else {
    return array('host' => '172.16.1.8',
		 'port' => '3307',
		 'user' => 'noblogsusr',
		 'password' => 'n0bl0gsdb4xpw!',
		 'db' => 'noblogs');
  }
}

function new_hash($dbid, $reversemap) {
  global $wpdb;
  $lookup = $wpdb->hash_map->lookup($dbid);
  $backend = $reversemap[$lookup];
  $result = array();
  if (preg_match('/^(.*):([0-9]*)$/', $backend['host'], $matches)) {
    $result['host'] = $matches[1];
    $result['port'] = $matches[2];
  }
  $result['user'] = $backend['user'];
  $result['password'] = $backend['password'];
  $result['db'] = $backend['name'];
  return $result;
}

function mysqlopts(&$attrs) {
  return ("-h" . $attrs['host'] . " -P" . $attrs['port'] . " -u" . $attrs['user']
	  . " '-p" . $attrs['password'] . "'");
}

function mysqlurl(&$attrs) {
  return ("mysql://" . $attrs['user'] . "@" . $attrs['host']
	  . ":" . $attrs['port'] . "/" . $attrs['db']);
}

function get_all_blogs() 
{
  global $wpdb;

//  $sql = "SELECT blog_id, domain, path FROM $wpdb->blogs WHERE public = 1 AND deleted = 0 AND archived = '0' ORDER BY domain, path";
  $sql = "SELECT blog_id, domain, path FROM $wpdb->blogs WHERE public = 0 ORDER BY domain, path";
  $result = $wpdb->get_results($sql);
  return ($result);
}

$hashmap = new Flexihash();
$reverse_backend_map = noblogs_load_backends(NOBLOGS_BACKEND_CONFIG, $hashmap);

$new_counts = array();
$moved_count = 0;
$blogs = get_all_blogs();
foreach ($blogs as $blog) {
  $blog_id = $blog->blog_id;

  $old_params = old_hash($blog_id);
  $old_dburi = mysqlurl($old_params);
  $new_params = new_hash($blog_id, $reverse_backend_map);
  $new_dburi = mysqlurl($new_params);

  if ($new_counts[$new_params['host']]) {
    $new_counts[$new_params['host']] += 1;
  } else { 
    $new_counts[$new_params['host']] = 1;
  }

  if ($old_dburi != $new_dburi) {
    echo "echo moving blog $blog_id from " . $old_params['host'] . " to " . $new_params['host'] . "\n";
    echo "tables=\$(mysql " . mysqlopts($old_params) . " " . $old_params['db'] . " -NBe \"show tables like 'wp\\_" . $blog_id . "\\_%'\")\n";
    echo "mysqldump --opt " . mysqlopts($old_params) . " " . $old_params['db'] . " \${tables} \\\n";
    echo "  | mysql " . mysqlopts($new_params) . " " . $new_params['db'] . "\n";
    $moved_count += 1;
  } else {
    echo "echo blog $blog_id stays on " . $old_params['host'] . "\n";
  }

}

echo "\n\n\nBlog distribution:\n";
print_r($new_counts);
print "\n $moved_count blogs moved\n";

