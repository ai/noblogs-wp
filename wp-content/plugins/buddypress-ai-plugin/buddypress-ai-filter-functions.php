<?php

function my_is_friend_check( $friend_id = false) {
global $bp;

if ( is_site_admin() )
return true;

if ( !is_user_logged_in() )
return false;

if (!$friend_id) {
$potential_friend_id = $bp->displayed_user->id;
} else {
$potential_friend_id = $friend_id;
}

if ( $bp->loggedin_user->id == $potential_friend_id )
return false;

if (friends_check_friendship_status($bp->loggedin_user->id, $potential_friend_id) == 'is_friend')
return true;

return false;
}


function my_denied_activities( $a, $activities ) {

//if admin we want to know
if ( is_site_admin() )
return $activities;

foreach ( $activities->activities as $key => $activity ) {
//new_member is the type name (component is 'profile')
if ( $activity->type != 'new_blog_post' && !is_user_logged_in()) {
unset( $activities->activities[$key] );

$activities->activity_count = $activities->activity_count-1;
$activities->total_activity_count = $activities->total_activity_count-1;
$activities->pag_num = $activities->pag_num -1;
}
if ( $activity->type == 'new_blog_post' && $activity->item_id == 4437 ) {
    unset( $activities->activities[$key] );
    $activities->activity_count = $activities->activity_count-1;
    $activities->total_activity_count = $activities->total_activity_count-1;
    $activities->pag_num = $activities->pag_num -1;
    }
}

/* Renumber the array keys to account for missing items */
$activities_new = array_values( $activities->activities );
$activities->activities = $activities_new;

return $activities;
}
?>
