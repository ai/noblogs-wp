<?php
/**
 * @package NoSpam
 */
/*
Plugin Name: NoSpam
Plugin URI: http://spam.noblogs.org/
Description: IP-Agnostic anti-spam filter.
Version: 0.1
Author: Autistici/Inventati
Author UTI: http://www.inventati.org/
License: GPLv2
*/

define('NOSPAM_VERSION', '0.1');

$nospam_api_url = "http://spam.noblogs.org/RPC2";

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
	exit;
}

function nospam_init() {
}
add_action('init', 'nospam_init');

function nospam_make_comment_for_training( $comment, $train ) {
  require_once('xmlrpc.inc');
  $s = array(
    'site' => new xmlrpcval(get_option('home'), 'string'),
    'name' => new xmlrpcval($comment->comment_author, 'string'),
    'comment' => new xmlrpcval($comment->comment_content, 'string'),
    'link' => new xmlrpcval($comment->comment_author_url, 'string'),
    'email' => new xmlrpcval($comment->comment_author_email, 'string'),
    'agent' => new xmlrpcval($comment->comment_agent, 'string'),
    'version' => new xmlrpcval(NOSPAM_VERSION, 'string'),
    'train' => new xmlrpcval($train, 'string'),
    );
  return new xmlrpcval($s, 'struct');
}

function nospam_make_comment( $commentdata ) {
  require_once('xmlrpc.inc');
  $comment = array(
    'site' => new xmlrpcval(get_option('home'), 'string'),
    'name' => new xmlrpcval($commentdata['comment_author'], 'string'),
    'comment' => new xmlrpcval($commentdata['comment_content'], 'string'),
    'link' => new xmlrpcval($commentdata['comment_author_url'], 'string'),
    'email' => new xmlrpcval($commentdata['comment_author_email'], 'string'),
    'agent' => new xmlrpcval($_SERVER['HTTP_USER_AGENT'], 'string'),
    'version' => new xmlrpcval(NOSPAM_VERSION, 'string'),
    );
  return new xmlrpcval($comment, 'struct');
}

function nospam_xmlrpc_query($method, $s) {
  global $nospam_api_url;
  require_once('xmlrpc.inc');
  $c = new xmlrpc_client($nospam_api_url);
  $r = $c->send(new xmlrpcmsg($method, array($s)));
  if ($r->faultCode()) {
    error_log('nospam_xmlrpc_query(): ERROR: ' . $r->faultString());
    return 0;
  }
  return $r->value()->scalarval();
}

function nospam_auto_check_comment( $commentdata ) {
  $comment = nospam_make_comment($commentdata);
  $r = nospam_xmlrpc_query('testComment', $comment);
  error_log('nospam_auto_check_comment(): ' . get_option('home') . ' -> ' . $r);
  if ($r && preg_match('/^SPAM:/', $r)) {
    add_filter('pre_comment_approved',
               create_function('$a', 'return \'spam\';'), 99);
  }
  return $commentdata;
}

add_action('preprocess_comment', 'nospam_auto_check_comment', 1);

function nospam_get_comment( $comment_id ) {
  global $wpdb;
  $comment_id = (int) $comment_id;
  $comment = $wpdb->get_row("SELECT * FROM $wpdb->comments WHERE comment_ID = '$comment_id'");
  if (!$comment) // it was deleted
    return 0;
  return $comment;
}

function nospam_submit_comment( $comment, $category ) {
  $cdata = nospam_make_comment_for_training($comment, $category);
  $c = nospam_xmlrpc_query('classifyComment', $cdata);
}

function nospam_submit_spam_comment( $comment_id ) {
  error_log('nospam_submit_spam_comment()');
  $comment = nospam_get_comment($comment_id);
  if (!$comment)
     return;
  if ($comment->comment_approved != 'spam')
     return;
  nospam_submit_comment($comment, 'spam');
}

function nospam_submit_nonspam_comment( $comment_id ) {
  error_log('nospam_submit_nonspam_comment()');
  $comment = nospam_get_comment($comment_id);
  if (!$comment)
     return;
  nospam_submit_comment($comment, 'ok');
}

function nospam_transition_comment_status( $new_status, $old_status, $comment ) {
  if ($new_status == $old_status)
    return;

  if ($new_status == 'spam') {
    nospam_submit_spam_comment($comment->comment_ID);
  } elseif ($old_status == 'spam' && ( $new_status == 'approved' || $new_status == 'unapproved')) {
    nospam_submit_nonspam_comment($comment->comment_ID);
  }
}

add_action('transition_comment_status', 'nospam_transition_comment_status', 10, 3);

