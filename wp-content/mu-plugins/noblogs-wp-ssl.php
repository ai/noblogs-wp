<?php
/*
Plugin Name: Noblogs Wp SSL
Plugin URI: https://git.autistici.org/ai/noblogs-wp-ssl
Description: Rewrite internal URLS to use HTTPS.
Version: 0.1
Author: Cloudflare, sand
License: GPLv2
 */

/*
  Heavily copied from: https://wordpress.org/plugins/cloudflare/
 */

if ( !function_exists('add_action') ) {
    echo "This is a Wordpress plugin and should not be called directly";
    exit;
}

function rewrite_links($buffer) {
        // replace href or src attributes within script, link, base, and img tags with just "//" for protocol
        $re     = "#(<(?:script|link|base|img|form|a)(?:[^>]*)(?:href|src|action)=[\"'])http://([^.]+\.noblogs\.org)#i";
        $subst  = "$1https://$2";
        return preg_replace($re, $subst, $buffer);
}


// This is a filter function that act on the post $content
function noblogs_wp_ssl_content_filter($content) {
    // skip rewrite if not on HTTPS
    if (empty($_SERVER['HTTPS'])) {
        return $content;
    }

    $return = rewrite_links($content);
    if ($return) {
        $content = $return;
    }

    return $content;
}

add_filter('the_content', 'noblogs_wp_ssl_content_filter');
