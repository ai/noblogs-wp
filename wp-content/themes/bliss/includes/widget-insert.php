<?php
/**
*
* Widget inserts
* Displays widget area by conditional tags
* @since 0.1
*
*/
	if(is_front_page() || is_home())
		$insert_id = __('Home Insert','bliss');
	elseif(is_attachment())
		$insert_id = __('Attachment Insert','bliss');
	elseif(is_single())
		$insert_id = __('Single Insert','bliss');
	elseif(is_page())
		$insert_id = __('Page Insert','bliss');
	elseif(is_category())
		$insert_id = __('Category Insert','bliss');
	elseif(is_tag())
		$insert_id = __('Tag Insert','bliss');
	elseif(is_search())
		$insert_id = __('Search Insert','bliss');
	elseif(is_author())
		$insert_id = __('Author Insert','bliss');
	elseif(is_archive())
		$insert_id = __('Archive Insert','bliss');
	elseif(is_404())
		$insert_id = __('404 Insert','bliss');
	else
		$insert_id = __('Home Insert','bliss');
?>

<div id="widget-insert-container">

	<div id='widget-insert'>

	<?php op_before_widget_insert(); // Before widgets hook ?>

	<?php
		if(dynamic_sidebar($insert_id)) :
		else :
			if(dynamic_sidebar(__('Home Insert','bliss'))) :
			endif;
		endif;
	?>

	<?php op_after_widget_insert(); // After widgets hook ?>

	</div>

</div>