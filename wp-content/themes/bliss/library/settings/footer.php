<div class="postbox close">

<h3><?php _e('Header &amp; Footer Settings','bliss'); ?></h3>
<div class="inside">

	<table class="form-table">
	<tr>
		<th>
			<label for="<?php echo $data['header_insert']; ?>"><?php _e('Header Insert:','bliss'); ?></label>
		</th>
		<td>
			<textarea id="<?php echo $data['header_insert']; ?>" name="<?php echo $data['header_insert']; ?>" cols="60" rows="5" style="width: 95%;"><?php echo str_replace('<','&lt;',stripslashes($val['header_insert'])); ?></textarea>
			<br />
			<?php _e('You can place XHTML and JavaScript here to have it inserted automatically into your theme.  Take note that this is placed within the &lt;head&gt;&lt;/head&gt; tags.  This is useful for adding JavaScript and alternate stylesheets.','bliss'); ?>
		</td>
	</tr>
	<tr>
		<th>
			<label for="<?php echo $data['footer_insert']; ?>"><?php _e('Footer Insert:','bliss'); ?></label>
		</th>
		<td>
			<textarea id="<?php echo $data['footer_insert']; ?>" name="<?php echo $data['footer_insert']; ?>" cols="60" rows="5" style="width: 95%;"><?php echo str_replace('<','&lt;',stripslashes($val['footer_insert'])); ?></textarea>
			<br />
			<?php _e('You can place XHTML and JavaScript here to have it inserted automatically into your theme.  If you have a script, such as one from Google Analytics, this could be useful.','bliss'); ?>
		</td>
	</tr>
	<tr>
		<th>
			<label for="<?php echo $data['site_footer']; ?>"><?php _e('Site Footer:','bliss'); ?></label>
		</th>
		<td>
			<input id="<?php echo $data['site_footer']; ?>" name="<?php echo $data['site_footer']; ?>" type="checkbox" <?php if($val['site_footer']) echo 'checked="checked"'; ?> value="true" /> 
			<?php _e('Check this if you want the theme to auto-generate your site\'s copyright and title in the footer.','bliss'); ?>
		</td>
	</tr>
	<tr>
		<th>
			<label for="<?php echo $data['wp_credit']; ?>"><?php _e('WordPress Love:','bliss'); ?></label>
		</th>
		<td>
			<input id="<?php echo $data['wp_credit']; ?>" name="<?php echo $data['wp_credit']; ?>" type="checkbox" <?php if($val['wp_credit']) echo 'checked="checked"'; ?> value="true" /> 
			<?php _e('Want to show your love of WordPress?  Check this and a link will be added to your footer back to WordPress.org.','bliss'); ?>
		</td>
	</tr>
	<tr>
		<th>
			<label for="<?php echo $data['jt_love']; ?>"><?php _e('Justin Tadlock Love:','bliss'); ?></label>
		</th>
		<td>
			<input id="<?php echo $data['jt_love']; ?>" name="<?php echo $data['jt_love']; ?>" type="checkbox" <?php if($val['jt_love']) echo 'checked="checked"'; ?> value="true" /> 
			<?php _e('Want to show your love of Justin Tadlock? Just check this and a link will be appended to your footer.  This is totally optional.  Really.','bliss'); ?>
		</td>
	</tr>
	<tr>
		<th>
			<label for="<?php echo $data['key']; ?>"><?php _e('Credit Removal:','bliss'); ?></label>
		</th>
		<td>
			<input id="<?php echo $data['key']; ?>" name="<?php echo $data['key']; ?>" value="<?php echo $val['key']; ?>" size="30" /> &mdash; 
			<input id="<?php echo $data['key_id']; ?>" name="<?php echo $data['key_id']; ?>" value="<?php echo $val['key_id']; ?>" size="2" maxlength="2" />
			<br />
			<?php _e('If you\'ve purchased credit removal, you can insert the credit removal key here to have the theme automatically remove the credit links in the footer.  Inputting this key helps me keep track of users that have legally purchased credit removal.','bliss'); ?>
		</td>
	</tr>

	</table>

	<p class="submit">
		<input type="submit" name="Submit" value="<?php _e('Update Options', 'bliss' ) ?>" />
		<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y" />
	</p>

</div>
</div>