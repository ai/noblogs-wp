<div class="postbox close">

<h3><?php _e('General Settings','bliss'); ?></h3>
<div class="inside">

	<table class="form-table">

	<tr>
		<th>
			<label for="<?php echo $data['feed_url']; ?>"><?php _e('Feedburner URL:','bliss'); ?></label>
		</th>
		<td>
			<input id="<?php echo $data['feed_url']; ?>" name="<?php echo $data['feed_url']; ?>" value="<?php echo $val['feed_url']; ?>" size="30" />
			<br />
			<?php _e('If you have a <a href="http://feedburner.com" title="Feedburner"> Feedburner</a> account, you can enter your feed address here to have the theme set your feed URL link.  If blank, the theme will default to your WordPress RSS feed.','bliss'); ?>
		</td>
	</tr>

	<tr>
		<th>
			<label for="<?php echo $data['author_bio']; ?>"><?php _e('Author Archive Bio:','bliss'); ?></label>
		</th>
		<td>
			<input id="<?php echo $data['author_bio']; ?>" name="<?php echo $data['author_bio']; ?>" type="checkbox" <?php if($val['author_bio']) echo 'checked="checked"'; ?> value="true" /> 
			<?php _e('Show author biographical information on author archives with an avatar?','bliss'); ?>
		</td>
	</tr>

	<tr>
		<th>
			<label for="<?php echo $data['print_style']; ?>"><?php _e('Print Stylesheet:','bliss'); ?></label>
		</th>
		<td>
			<input id="<?php echo $data['print_style']; ?>" name="<?php echo $data['print_style']; ?>" type="checkbox" <?php if($val['print_style']) echo 'checked="checked"'; ?> value="true" /> 
			<?php _e('Select this to have the theme automatically include a print stylesheet.','bliss'); ?>
		</td>
	</tr>

	<tr>
		<th>
			<label for="<?php echo $data['home']; ?>"><?php _e('Home Display:','bliss'); ?></label>
		</th>
		<td>
			<select id="<?php echo $data['home']; ?>" name="<?php echo $data['home']; ?>">
				<option <?php if($val['home'] == __('Full Posts','bliss')) echo ' selected="selected"'; ?>>
					<?php _e('Full Posts','bliss'); ?>
				</option>
				<option <?php if($val['home'] == __('Excerpts','bliss')) echo ' selected="selected"'; ?>>
					<?php _e('Excerpts','bliss'); ?>
				</option>
			</select>
			<br />
			<?php _e('Select how you want your home page\'s posts to display.','bliss'); ?>
		</td>
	</tr>

	<tr>
		<th>
			<label for="<?php echo $data['archives']; ?>"><?php _e('Archives Display:','bliss'); ?></label>
		</th>
		<td>
			<select id="<?php echo $data['archives']; ?>" name="<?php echo $data['archives']; ?>">
				<option <?php if($val['archives'] == __('Full Posts','bliss')) echo ' selected="selected"'; ?>>
					<?php _e('Full Posts','bliss'); ?>
				</option>
				<option <?php if($val['archives'] == __('Excerpts','bliss')) echo ' selected="selected"'; ?>>
					<?php _e('Excerpts','bliss'); ?>
				</option>
			</select>
			<br />
			<?php _e('Select how you want your archived posts to display.','bliss'); ?>
		</td>
	</tr>

	<tr>
		<th>
			<label for="<?php echo $data['home_custom_1']; ?>"><?php _e('Home Insert 1:','bliss'); ?></label>
		</th>
		<td>
			<textarea id="<?php echo $data['home_custom_1']; ?>" name="<?php echo $data['home_custom_1']; ?>" cols="60" rows="5" style="width: 95%;"><?php echo str_replace('<','&lt;',stripslashes($val['home_custom_1'])); ?></textarea>
			<br />
			<?php _e('You can input any XHTML you want to here.  You might also have to add CSS rules for this in your stylesheet.','bliss'); ?> 
			<?php _e('This will be added before your home page\'s main content.','bliss'); ?>
		</td>
	</tr>

	<tr>
		<th>
			<label for="<?php echo $data['home_custom_2']; ?>"><?php _e('Home Insert 2:','bliss'); ?></label>
		</th>
		<td>
			<textarea id="<?php echo $data['home_custom_2']; ?>" name="<?php echo $data['home_custom_2']; ?>" cols="60" rows="5" style="width: 95%;"><?php echo str_replace('<','&lt;',stripslashes($val['home_custom_2'])); ?></textarea>
			<br />
			<?php _e('You can input any XHTML you want to here.  You might also have to add CSS rules for this in your stylesheet.','bliss'); ?> 
			<?php _e('This will be added after your home page\'s main content.','bliss'); ?>
		</td>
	</tr>

	</table>

	<p class="submit">
		<input type="submit" name="Submit" value="<?php _e('Update Options', 'bliss' ) ?>" />
		<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y" />
	</p>
</div>
</div>