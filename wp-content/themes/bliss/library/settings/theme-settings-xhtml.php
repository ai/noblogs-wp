<?php
// Array of home sections to shorten code later
// Reuse and recycle for the good of the WP environment
	$home_sections_arr = array(
		__('Excerpts','bliss'),
		__('Full Posts','bliss'),
	);
	$home_arr_val = array_values($home_sections_arr);

// Theme data
	$theme_data = get_theme_data(TEMPLATEPATH . '/style.css');

// Home sections array
	$home_sec_arr = array(
		array(__('Home Section 1:','bliss'), 'home1')
	);

// Get all category slugs for use
	$all_cat_slugs_arr = op_all_cat_slugs();

// Get all category names for use
	$all_cats_arr = op_all_cats();

// Get all tags for use
	$all_tags_arr = op_all_tags();
?>

<div id="poststuff" class="dlm">

	<form name="form0" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" style="border:none;background:transparent;">

	<?php
		include(OP_SETTINGS . '/about.php');
		include(OP_SETTINGS . '/general.php');
		include(OP_SETTINGS . '/footer.php');
	?>

		<script type="text/javascript">
			<!--
			jQuery('.postbox h3').prepend('<a class="togbox">+</a> ');
			jQuery('.postbox h3').click( function() { jQuery(jQuery(this).parent().get(0)).toggleClass('closed'); } );
			jQuery('.postbox.close').each(function(){ jQuery(this).addClass("closed"); });
			//-->
		</script>

	</form>

</div>

</div>