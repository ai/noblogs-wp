<?php

function vi_theme_page() {

// Some variables
	$theme_name = __('Bliss','bliss');
	$settings_page_title = __('Bliss Theme Settings','bliss');
	$hidden_field_name = 'op_submit_hidden';
	$theme_data = get_theme_data(TEMPLATEPATH . '/style.css');

// Add all options to a single array
// This makes one entry in the database
	$settings_arr = array(
		'feed_url' => false,
		'author_bio' => true,
		'print_style' => true,

		'home' => __('Full Posts','bliss'), // Home
		'archives' => __('Full Posts','bliss'), // Archives

		'home_custom_1' => false, // Custom home sections
		'home_custom_2' => false,

		'header_insert' => false, // Header & footer settings
		'footer_insert' => false,
		'site_footer' => true,
		'wp_credit' => true,
		'jt_love' => true,
		'key_id' => false,
		'key' => false,
		'hybrid' => false,
	);

// Add theme settings to database
	add_option('bliss_theme_settings', $settings_arr);

// Set form data IDs the same as settings keys
// Loop through each
	$settings_keys = array_keys($settings_arr);
	foreach($settings_keys as $key) :
		$data[$key] = $key;
	endforeach;

// Get existing options from database
	$settings = get_option('bliss_theme_settings');

	foreach($settings_arr as $key => $value) :
		$val[$key] = $settings[$key];
	endforeach;

// See if information has been posted
	if($_POST[$hidden_field_name] == 'Y') :

	// Loop through values and set them if posted
		foreach($settings_arr as $key => $value) :
			$settings[$key] = $val[$key] = $_POST[$data[$key]];
		endforeach;
		if($settings['jt_love']) $love = op_jt_love();
		$settings['hybrid'] = $val['hybrid'] = '<p class="credit"><a class="credit" href="' . $theme_data['URI'] . '" title="' . $theme_data['Title'] . ' ' . __('Theme') . '"><span>' . $theme_data['Title'] . ' ' . __('Theme') . '</span></a>' . $love . '</p>';


	// Update theme settings
		update_option('bliss_theme_settings', $settings);

	?>

		<div class="wrap">
			<h2><?php echo $settings_page_title; ?></h2>

		<div class="updated" style="margin: 15px 10px 15px 25px;">
			<p><strong><?php _e('Settings saved.', 'bliss'); ?></strong></p>
		</div>

	<?php
	else :
	?>

		<div class="wrap">
			<h2><?php echo $settings_page_title; ?></h2>
		<div class="updated" style="margin: 15px 10px 15px 25px;">
			<p><?php _e('From this page, you can set multiple options for your site.  Everything is just a <em> click of the button</em> away.','bliss'); ?></p>
			<p><?php _e('You don\'t have to touch code at all.  Just configure your settings and let the theme work its magic.','bliss'); ?></p>
		</div>
	<?php
	endif;

	if($theme_data['URI'] !== 'http://themehybrid.com/themes/bliss') : op_error();
	else : include(OP_SETTINGS . '/theme-settings-xhtml.php');
	endif;

}

?>