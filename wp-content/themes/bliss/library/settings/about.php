<div class="postbox close">
<h3><?php _e('About This Theme','bliss'); ?></h3>

<div class="inside">
	<table class="form-table">

	<tr>
		<th><?php _e('Theme Description:','bliss'); ?></th>
		<td><?php echo $theme_data['Description']; ?></td>
	</tr>
	<tr>
		<th><?php _e('Theme Version:','bliss'); ?></th>
		<td><?php echo $theme_data['Title']; ?> <?php echo $theme_data['Version']; ?></td>
	</tr>
	<tr>
		<th><?php _e('Theme Documentation:','bliss'); ?></th>
		<td><a href="<?php echo $theme_data['URI']; ?>" title="<?php _e('Theme Documentation','bliss'); ?>"><?php _e('Theme Documentation','bliss'); ?></a></td>
	</tr>
	<tr>
		<th><?php _e('Theme Support:','bliss'); ?></th>
		<td><a href="http://themehybrid.com/support" title="<?php _e('Visit the support forums.','bliss'); ?>"><?php _e('Visit the support forums.','bliss'); ?></a></td>
	</tr>
	<tr>
		<th><?php _e('Donate:','bliss'); ?></th>
		<td><a href="http://themehybrid.com/contribute" title="<?php _e('Contribute to this project','bliss'); ?>"><?php _e('Donations','bliss'); ?></a> <?php _e('help keep theme downloads and support free.','bliss'); ?></td>
	</tr>

	</table>
</div>
</div>