<?php

// Include files
	include(OP_SETTINGS . '/theme-settings-admin.php');

// Add actions
	add_action('admin_menu', 'op_add_pages');
	add_action('admin_head', 'op_admin_enqueue_style');

/***********************************************************
* op_add_pages() - Gets all theme admin menu pages
***********************************************************/
function op_add_pages() {
	add_theme_page(__('Bliss Theme Settings','bliss'), __('Bliss Settings','bliss'), 10, 'theme-settings.php', vi_theme_page);
}

/***********************************************************
* op_admin_css() - Adds admin CSS
***********************************************************/
function op_admin_enqueue_style() {
	wp_enqueue_style('bliss_admin_css', OP_CSS . '/theme-settings.css', false, false, 'screen');
	wp_print_styles(array('bliss_admin_css'));
}
?>