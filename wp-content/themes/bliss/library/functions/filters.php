<?php
/***********************************************************
* Theme filters
* @since 1.3
* Functions that can be filtered
***********************************************************/

function op_header_feed() {
	global $settings;

	if($settings['feed_url']) $url = $settings['feed_url'];
	else $url = get_bloginfo('rss2_url');

	echo "<div id='feed'>\n\t\t";

	$feed = "\t<ul>";
	$feed .= '<li class="feed-url"><a href="' . $url . '" title="' . __('Subscribe to the feed','bliss') . '"><span>' . __('Subscribe by RSS','bliss') . '</span></a></li>';
	if($settings['feed_id'])
		$feed .= '<li class="feed-email"><a href="http://www.feedburner.com/fb/a/emailverifySubmit?feedId=' . $settings['feed_id'] . '" title="' . __('Subscribe by email','bliss') . '"><span>' . __('Subscribe by email','bliss') . '</span></a></li>';
	$feed .= '</ul>';
	echo apply_filters('op_header_feed', $feed);

	echo "\n\t\t</div>\n";
}

/***********************************************************
* op_page_nav() - Displays main navigation
* @since 1.3
* @filter
***********************************************************/
function op_page_nav() {

	echo "<div id='navigation'>\n\t\t";

		// Allow users to filter navigation
			$nav = '<ul id="nav">';
			$nav .= str_replace(array("\t","\n","\r"), '', wp_list_pages('title_li=&depth=1&sort_column=menu_order&echo=0'));
			$nav .= '</ul>';
			echo apply_filters('op_page_nav', $nav);
		// Get search
			op_main_search();

	echo "\t</div>\n";
}

/***********************************************************
* op_cat_nav() - Displays sub navigation
* @since 1.3
* @filter
***********************************************************/
function op_cat_nav() {
	echo "\n\t<div id='cat-navigation'>\n\t\t";
		$nav = '<ul id="sub-nav">';
		$nav .= str_replace(array("\t","\n","\r"), '', wp_list_categories('title_li=&use_desc_for_title=0&depth=1&orderby=name&hierarchical=1&echo=0'));
		$nav .= '</ul>';
		echo apply_filters('op_cat_nav',$nav);
	echo "\n\t</div>\n";
}


/***********************************************************
* op_main_search() - Displays search in main nav
* @since 1.3
* @filter
***********************************************************/

function op_main_search() {
	global $settings;
	
	echo "\n\t\t<div id='search'>\n\t\t\t";
		$search = '<form method="get" id="searchform" action="' . get_bloginfo("home") . '">';
		$search .= '<div>';
		$search .= '<input class="search" type="text" name="s" id="s" tabindex="7" value="' . __('Search this site...','bliss') . '" onfocus="if(this.value==this.defaultValue)this.value=\'\';" onblur="if(this.value==\'\')this.value=this.defaultValue;" />';
		$search .= '<input class="search-submit" name="submit" type="submit" id="search-submit" tabindex="8" value="' . __('Go','bliss') . '" />';
		$search .= '</div>';
		$search .= '</form>';
		echo apply_filters('op_main_search',$search);
	echo "\n\t\t</div>\n";
}

?>