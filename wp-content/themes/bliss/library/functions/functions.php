<?php
/********************************************************
* op_error() - A simple error output
* @since 1.3
********************************************************/
function op_error() {
	_e('Oooops! It looks like you\'ve messed something up.  Try undoing your last edit to see if you can fix it.  If it\'s still broken, check out the support forums.','bliss');
}

/***********************************************************
* op_all_cats() - Returns an array of available categories
* @since 1.2
***********************************************************/
function op_all_cats() {
	$all_cats = get_all_category_ids();
	foreach($all_cats as $key => $value) :
		$all_cats[$key] = get_cat_name($all_cats[$key]);
		$all_cats[$key] = str_replace("&#038;", "&", $all_cats[$key]);
		$all_cats[$key] = str_replace("&amp;","&", $all_cats[$key]);
	endforeach;
	$all_cats['none'] = false;
	return $all_cats;
}

/***********************************************************
* op_all_cat_slugs() - Returns an array of available category slugs
* @since 1.2
***********************************************************/
function op_all_cat_slugs() {
	$cats = get_categories("hierarchical=0");
	foreach($cats as $key) :
		$all_cats[] = $key->category_nicename;
	endforeach;
	$all_cats['none'] = false;
	return $all_cats;
}

/***********************************************************
* op_all_tags() - Returns an array of the available tags (slugs)
* @since 1.1 - 1.2
***********************************************************/
function op_all_tags() {
	$all_tags = wp_tag_cloud('number=0&format=array');
	if($all_tags) :
		foreach($all_tags as $key => $value) :
			$value = strip_tags(stripslashes($value));
			$value = strtolower($value);
			$value = str_replace(array("&nbsp;", " "), "-", $value);
			$all_tags[$key] = $value;
		endforeach;
	endif;
	$all_tags['none'] = false;
	return $all_tags;
}

/***********************************************************
* op_all_tag_names() - Returns an array of the available tags (names)
* @since 1.1 - 1.2
***********************************************************/
function op_all_tag_names() {
	$all_tags = wp_tag_cloud('number=0&format=array');
	if($all_tags) :
		foreach($all_tags as $key => $value) :
			$value = strip_tags(stripslashes($value));
			$all_tags[$key] = $value;
		endforeach;
	endif;
	$all_tags['none'] = false;
	return $all_tags;
}

/***********************************************************
* op_all_authors() - Returns an array of the available authors
* @since 1.1 - 1.2
***********************************************************/
function op_all_authors() {
	global $wpdb;
	$query = "SELECT ID, user_nicename from $wpdb->users ORDER BY user_nicename";
	$user_ids = $wpdb->get_results($query);
	foreach($user_ids as $user_id => $value) :
		$user = get_userdata($user_id);
		if($user->user_level > 0) $all_authors[$user->user_login] = $user->user_login;
	endforeach;
	$all_authors['none'] = false;
	return $all_authors;
}

/***********************************************************
* op_all_authors_ids() - Returns an array of the available author IDs
* @since 1.1 - 1.2
***********************************************************/
function op_all_authors_ids() {
	global $wpdb;
	$user_ids = $wpdb->get_results("SELECT COUNT(b.ID) AS postsperuser, a.ID as post_id, display_name, user_nicename, b.ID as ID FROM wp_posts AS a LEFT join wp_users AS b ON a.post_author = b.ID GROUP BY b.ID ORDER BY display_name");
	//$query = "SELECT ID, user_nicename from $wpdb->users ORDER BY user_nicename";
	//$user_ids = $wpdb->get_results($query);
	foreach($user_ids as $user_id => $value) :
		$user = get_userdata($user_id);
		$all_ids[$user->ID] = $user->ID;
		$all_names[$user->display_name] = $user->display_name;
		$all_nice[$user->user_nicename] = $user->user_nicename;
	endforeach;
	return array($all_ids, $all_names, $all_nice);
}

/***********************************************************
* op_author_id() - Returns author ID by nicename
* @since 1.1 - 1.2
***********************************************************/
function op_author_id($author_name) {
	//$author_name = str_replace("-", " ", $author_name); 
	$curauth = get_userdatabylogin($author_name);
	$author_id = $curauth->ID;
	return $author_id;
}

/***********************************************************
* op_author_nicename() - Returns author nicename
* @since 1.1 - 1.2
***********************************************************/
function op_author_nicename($author_name) {
	$curauth = get_userdatabylogin($author_name);
	$nicer = $curauth->user_nicename;
	return $nicer;
}

/***********************************************************
* op_author_display_name() - Returns author display name by nicename
* @since 1.1 - 1.2
***********************************************************/
function op_author_display_name($author_name) {
	//$author_name = str_replace("-", " ", $author_name);
	$curauth = get_userdatabylogin($author_name);
	$display = $curauth->display_name;
	return $display;
}

/***********************************************************
* op_author_cloud() - Returns a cloud of authors
* @since 1.2
* @fix 1.3 removed 53 db queries
***********************************************************/
function op_author_cloud() {
	global $wpdb;
	$author_cloud = $wpdb->get_results("SELECT COUNT(b.ID) AS postsperuser, a.ID as post_id, display_name, user_nicename, b.ID as ID FROM wp_posts AS a LEFT join wp_users AS b ON a.post_author = b.ID GROUP BY b.ID ORDER BY display_name");
	foreach($author_cloud as $user) :
		$user_ids[] = $user->ID;
		$user_nicenames[] = $user->user_nicename;
		$user_name = $user->display_name;
		$count = get_usernumposts($user->ID);
		if($count >= 0) $counts[$user_name] = $count;
	endforeach;

	$dist = max($counts) - min($counts);
	if($dist <= 0) $dist = 1;
	$font = $dist / 10;
	$i = 0;

	foreach($counts as $authname => $count) :
		$link = get_author_posts_url($user_ids[$i]);
		echo "\n<a href=\"$link\" title=\"$count "; _e('posts by','bliss'); echo " $authname\" style=\"font-size:".(10 + ceil($count/$font))."px\">$authname</a> ";
		$i++;
	endforeach;
}

/***********************************************************
* op_category_cloud() - Returns a cloud of categories
* @since 1.2
***********************************************************/
function op_category_cloud() {
	$cat_cloud = get_categories("hierarchical=0");
	foreach($cat_cloud as $cat) :
		$cat_link = get_category_link($cat->cat_ID);
		$cat_name = $cat->cat_name;
		$count = $cat->category_count;
		if($count >= 0) {
			$counts[$cat_name] = $count;
			$cat_links[$cat_name] = $cat_link;
		}
	endforeach;

	$dist = max($counts) - min($counts);
	if($dist <= 0) $dist = 1;
	$font = $dist / 10;

	foreach($counts as $cat_name => $count) :
		$link = $cat_links[$cat_name];
		echo "\n<a href=\"$link\" title=\"$count "; _e('posts filed under','bliss'); echo " $cat_name\" style=\"font-size:".(10 + ceil($count/$font))."px\">$cat_name</a> ";
	endforeach;
}

/***********************************************************
* op_bookmark_cloud() - Returns a cloud of bookmarks by rating
* @since 1.3
***********************************************************/
function op_bookmark_cloud() {
	$bookmarks = get_bookmarks();
	foreach($bookmarks as $book) :
		$book_name = $book->link_name;
		if($count >= 0) {
			$counts[$book_name] = $book->link_rating;
			$book_links[$book_name] = $book->link_url;
			$book_rels[$book_name] = $book->link_rel;
		}
	endforeach;

	$dist = max($counts) - min($counts);
	if($dist <= 0) $dist = 1;
	$font = $dist / 10;

	foreach($counts as $book_name => $count) :
		$link = $book_links[$book_name];
		$rel = $book_rels[$book_name];
		echo "\n<a href=\"$link\" title=\"$book_name\" style=\"font-size:".(10 + ceil($count/$font))."px\" ";
		if($rel == true) echo "rel=\"$rel\""; else echo "rel=\"bookmark\"";
		echo ">$book_name</a> ";
	endforeach;
}
?>