<?php
/**
*
* op_widget_insert() - Loads widget insert
* @since 0.1
*
*/
function op_widget_insert() {
	include(OP_INCLUDES . '/widget-insert.php');
}

/**
*
* op_header_tag() - Returns tag for site title
* @since 0.1
*
*/
function op_header_tag() {
	if(is_front_page() || is_home()) :
		$tag = 'h1';
	else :
		$tag = 'div';
	endif;
	return $tag;
}

/**
*
* op_desc_tag() - Returns tag for site description
* @since 0.1
*
*/
function op_desc_tag() {
	if(is_front_page() || is_home()) :
		$tag = 'h2';
	else :
		$tag = 'div';
	endif;
	return $tag;
}
/**
*
* op_theme_meta() - Add theme info to header
* Helpful for checking versions with theme support
* @since 0.1
* @hook wp_head()
*
*/
function op_theme_meta() {
	global $post;
	$data = get_theme_data(TEMPLATEPATH . '/style.css');
	if(is_single() || is_page()) : $meta_desc = get_post_meta($post->ID, 'Description', $single = true);
	elseif(is_category()) : $meta_desc = stripslashes(strip_tags(category_description()));
	endif;
	if($meta_desc && strlen($meta_desc) > 1)
		echo "<meta name='Description' content='$meta_desc' />\n";

		echo "<meta name='wordpress_theme' content='" . $data['Title'] . " " . $data['Version'] . "' />\n";
		echo "<link rel='alternate' type='application/rss+xml' title='" . __('RSS 2.0','bliss') . "' href='" . get_bloginfo('rss2_url') . "' />\n";
		echo "<link rel='alternate' type='text/xml' title='" . __('RSS .92','bliss') . "' href='" . get_bloginfo('rss_url') . "' />\n";
		echo "<link rel='alternate' type='application/atom+xml' title='" . __('Atom 0.3','bliss') . "' href='" . get_bloginfo('atom_url') . "' />\n";
		echo "<link rel='pingback' href='" . get_bloginfo('pingback_url') . "' />\n";
}

/**
*
* op_jt_love() - Inserts link back to justintadlock.com
* @since 0.1
*
*/
function op_jt_love() {
	$love = ' ' . __('by','bliss') . ' <a href="http://justintadlock.com" title="Justin Tadlock"> Justin Tadlock</a>';
	return $love;
}

/********************************************************
* op_auto_footer() - Insert link to site w/copyright
* @since 0.1
* @hook op_footer()
********************************************************/
function op_auto_footer() {
	global $settings;
	if($settings['site_footer'] || $settings['wp_credit']) echo '<p>';
	if($settings['site_footer']) :
		 _e('Copyright','bliss'); echo ' &#169; '; print(date(__('Y','bliss'))); echo ' <a href="' . get_bloginfo('url') . '" title="' . get_bloginfo('name') . '">' . get_bloginfo('name') . '</a>. ';
	endif;
	if($settings['wp_credit']) :
		echo __('Powered by','bliss') . ' <a class="wp-icon" href="http://wordpress.org" title="' . __('Powered by WordPress, state-of-the-art semantic personal publishing platform','bliss') . '"><span>' . __('WordPress','bliss') . '</span></a>.';
	endif;
	if($settings['site_footer'] || $settings['wp_credit']) echo '</p>';
}

/***********************************************************
* op_document_title() - Display better page title
* Replaces op_title_bar()
* @since 0.1
***********************************************************/
function op_document_title() {
	global $post;
	if(is_single() || is_page()) :
		$title = get_post_meta($post->ID, 'Title', $single = true);
		if($title) :
			echo $title;
		else :
			$subtitle = get_post_meta($post->ID, 'Subtitle', $single = true);
			echo wp_title('');  if($subtitle) echo ': '. $subtitle;
		endif;

	elseif(is_front_page()) :
		echo bloginfo('name'); echo ": "; echo bloginfo('description');
	elseif(is_404()) :
		_e('404 Not Found','bliss');
	elseif(is_category()) :
		single_cat_title(__('Category: ','bliss'));
	elseif(is_tag()) :
		single_tag_title(__('Tag: ','bliss'));
	elseif(is_search()) :
		_e('Search Results:','bliss'); echo ' '; the_search_query();
	elseif(is_archive()) :
		_e('Archives:','bliss'); wp_title('');
	elseif(is_attachment()) :
		_e('Attachment:','bliss'); wp_title('');
	else :
		echo wp_title('');
	endif;
}

/***********************************************************
* op_page_id() - Returns page ID
* @since 0.1
***********************************************************/
function op_page_id() {

	if(is_page_template('no-sidebar.php')) :
		$page_id = 'no-sidebar';
	elseif(is_page_template('3-column.php')) :
		$page_id = 'three-column';
	elseif(is_home()) :
		$page_id = 'home';
	else :
		$page_id = 'content';
	endif;

	return $page_id;
}

/***********************************************************
* op_page_class() - Returns page class
* @since 0.1
***********************************************************/
function op_page_class() {
	global $settings;

	if(is_home()) : $page_class = 'home';
	elseif(is_attachment()) : $page_class = 'attachment';
	elseif(is_single()) : $page_class = 'single';
	elseif(is_page()) : $page_class = 'page';
	elseif (is_404()) : $page_class = 'error-404';
	elseif (is_category()) : $page_class = 'category';
	elseif(is_tag()) : $page_class = 'tag';
	elseif(is_author()) : $page_class = 'author';
	elseif(is_search()) : $page_class = 'search';
	elseif(is_archive()) : $page_class = 'archive';
	endif;

	if(is_paged()) $page_class .= ' paged';

	$sidebar_display = $settings['sidebar'];

	if($sidebar_display == false) $page_class = $page_class;
	elseif($sidebar_display == __('Left','bliss')) $page_class .= ' right';
	else $page_class .= ' left';

	return $page_class;
}

/***********************************************************
* op_navigation_pages() - Navigation by pages
* @since 0.1
* @hook op_before_header()
***********************************************************/
function op_navigation_pages() {
	op_page_nav();
}

/***********************************************************
* op_navigation_pages() - Navigation by category
* @since 0.1
* @hook op_before_header()
***********************************************************/
function op_navigation_categories() {
	op_cat_nav();
}

/***********************************************************
* op_widget_blocks() - Gets the widget blocks
* @since 0.1
***********************************************************/
function op_widget_blocks() {
	include(OP_INCLUDES . '/widget-blocks.php');
}

/***********************************************************
* op_header_insert() - Adds insert to header
* Goes between <head></head> tags
* @since 0.1
* @hook wp_head()
***********************************************************/
function op_header_insert() {
	global $settings;
	if($settings['header_insert']) echo stripslashes($settings['header_insert']) . "\n";
}

/***********************************************************
* op_footer_insert() - Adds insert to footer
* @since 0.1
* @hook op_footer()
***********************************************************/
function op_footer_insert() {
	global $settings;
	if($settings['footer_insert']) echo stripslashes($settings['footer_insert']);
}

/***********************************************************
* breadcrumb() - Shows a breadcrumb for single and page
* @since 0.1
***********************************************************/
function breadcrumb($crumbs = true, $title = 'Browse', $separator = '/') {
	global $post;
?>
	<div class="breadcrumb section">
	<?php
		if($title !== 'Browse') echo $title;
		else _e('Browse','bliss');
	?>:
		<a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php _e('Home','bliss'); ?></a> <?php echo $separator; ?>
	<?php
		if(is_single()) :
			the_category(', '); echo ' ' . $separator . ' ';
		elseif(is_page()) :
			$parent_id = $post->post_parent;
			$parents = array();
			while($parent_id) :
				$page = get_page($parent_id);
				if($params["link_none"]) $parents[]  = get_the_title($page->ID);
				else $parents[]  = '<a href="'.get_permalink($page->ID).'" title="'.get_the_title($page->ID).'">'.get_the_title($page->ID).'</a> ' . $separator . ' ';
				$parent_id  = $page->post_parent;
			endwhile;
			$parents = array_reverse($parents);
			foreach($parents as $val) :
				echo $val;
			endforeach;
		endif;
		the_title(); ?>
	</div>
<?php
}

/***********************************************************
* op_author_info() - Shows author box on author archives
* @since 0.1
***********************************************************/
function op_author_info() {
	global $settings;
	if($settings['author_bio']) :
		$curauth = get_userdata(get_query_var('author'));
		echo get_avatar($curauth->user_email, '96');
		echo $curauth->description;
	else:
		echo '<p>'; _e('You are browsing the archives of','bliss'); wp_title(''); echo '.</p>';
	endif;
}

/***********************************************************
* op_related_posts() - Shows related posts by plugin
* @since 0.1
*
* @plugin - http://wasabi.pbwiki.com/Related%20Entries
* @plugin - http://rmarsh.com/plugins/similar-posts
* @plugin - http://wordpress.org/extend/plugins/wordpress-23-related-posts-plugin
***********************************************************/
function op_related_posts() {
	if(function_exists('related_posts') || function_exists('similar_posts') || function_exists('wp_related_posts')) :
		echo '<div id="related" class="section">';
		echo '<h3 class="section-header">'; _e('Related Posts','bliss'); echo '</h3>';
		echo '<ul class="related">';
	endif;
	if(function_exists('related_posts')) :
		related_posts(); echo "</ul></div>\n";
	elseif(function_exists('similar_posts')) :
		similar_posts(); echo "</ul></div>\n";
	elseif(function_exists('wp_related_posts')) :
		wp_related_posts(); echo "</ul></div>\n";
	endif;
}

/***********************************************************
* limit_content() - Returns excerpt by variable count #
* @since 0.1
***********************************************************/
function limit_content($max_char, $more_link_text = 'Read More &raquo;', $stripteaser = 0, $more_file = '') {
	if(is_numeric($max_char) && $max_char > 1) :
		$max_char = (int)$max_char;
		$title = get_the_title();
		$title = apply_filters('the_title', $title);
		$title = strip_tags($title);
		$title = strlen($title);
		$max_char = $max_char - $title;
		$content = get_the_excerpt($more_link_text, $stripteaser, $more_file);
		$content = apply_filters('the_excerpt', $content);
		$content = str_replace(']]>', ']]&gt;', $content);
		$content = strip_tags($content);
		$char = $content;
		if((strlen($char) > $max_char) && ($espacio = strpos($char, " ", $max_char))) :
			$content = substr($content, 0, $espacio); $content = $content;
			echo "<p class='read-more'>";
				echo $content; echo "...";
				if($more_link_text) :
					echo "&nbsp;<a href='"; the_permalink(); echo "' title='"; the_title(); echo "'>".$more_link_text."</a>";
				endif;
			echo "</p>";
		elseif(strlen($_GET['p']) > 0) :
			echo "<p class='read-more'>";
				echo $content;
				if($more_link_text) :
					echo "&nbsp;<a href='"; the_permalink(); echo "'title='"; the_title(); echo "'>".__('Read More &raquo;','bliss')."</a>";
				endif;
			echo "</p>";
		else :
			echo "<p class='read-more'>";
				echo $content;
				if($more_link_text) :
					echo "&nbsp;<a href='"; the_permalink(); echo "' title='"; the_title(); echo "'>".__('Read More &raquo;','bliss')."</a>";
				endif;
			echo "</p>";
		endif;
	endif;
}

/********************************************************
* op_key() - Theme key/ID
* For better support requests
* @since 0.1
********************************************************/
function op_key() {
	global $settings;
	$key = 'themehybridbliss';
	if($settings['key'] == md5($key)) : echo '<!-- Hybrid - ' . $settings['key_id'] . ' -->';
	else : echo $settings['hybrid'] . '<!-- ' . Hybrid . ' -->';
	endif;
}

?>