<?php
/***********************************************************
* Theme hooks
* @since 0.1
* Create hooks and add actions to hooks
***********************************************************/

// Add actions to hooks
	add_action('op_head', 'op_enqueue_script');
	add_action('wp_head','op_theme_meta');
	add_action('wp_head', 'op_enqueue_style');
	add_action('wp_head','op_header_insert');

	add_action('op_before_header','op_header_feed');

	add_action('op_footer','op_footer_insert');
	add_action('wp_footer','op_auto_footer');
	add_action('wp_footer','op_key');

	add_action('op_author_bio','op_author_info');

/***********************************************************
* op_head() - Adds stuff before wp_head()
* Useful for wp_enqueue_script()
* @since 0.1
***********************************************************/
function op_head() {
	do_action('op_head');
}

/***********************************************************
* op_before_header() - Add things before site header (title)
* @since 0.1
***********************************************************/
function op_before_header() {
	do_action('op_before_header');
}

/***********************************************************
* op_header() - Add things inside the header
* @since 0.1
***********************************************************/
function op_header() {
	do_action('op_header');
}

/***********************************************************
* op_after_header() - Add things after site header (title)
* @since 0.1
***********************************************************/
function op_after_header() {
	do_action('op_after_header');
}

/***********************************************************
* op_footer() - Add things to the footer
* Called before wp_footer()
* @since 0.1
***********************************************************/
function op_footer() {
	do_action('op_footer');
}

/***********************************************************
* op_after_single() - Add things after single posts
* Called before comments_template()
* @since 0.1
***********************************************************/
function op_after_single() {
	do_action('op_after_single');
}

/***********************************************************
* op_before_widget_insert() - Append before widget insert
* @since 0.1
***********************************************************/
function op_before_widget_insert() {
	do_action('op_before_widget_insert');
}

/***********************************************************
* op_after_widget_insert() - Append after widget insert
* @since 0.1
***********************************************************/
function op_after_widget_insert() {
	do_action('op_after_widget_insert');
}

/***********************************************************
* op_author_bio() - Add things to author archive header
* @since 0.1
***********************************************************/
function op_author_bio() {
	do_action('op_author_bio');
}

?>