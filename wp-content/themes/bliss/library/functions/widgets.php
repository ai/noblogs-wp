<?php
/************************************************
* Register the widget areas and widgets.
* Can add new widget areas in child theme's functions.php.
************************************************/

/************************************************
* Create sidebar names
************************************************/
	$widget_insert = array(
		__('Home Insert','bliss'),
		__('Single Insert','bliss'),
		__('Archive Insert','bliss'),
		__('Attachment Insert','bliss'),
		__('Page Insert','bliss'),
		__('Category Insert','bliss'),
		__('Tag Insert','bliss'),
		__('Search Insert','bliss'),
		__('Author Insert','bliss'),
		__('404 Insert','bliss'),
	);

/************************************************
* Widgets display for each widget insert
************************************************/
	foreach($widget_insert as $insert) :
		register_sidebar(array(
			'name' => $insert,
			'before_widget' => '<div class="menu">',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="section-header">',
			'after_title' => '</h2>', ));
	endforeach;
?>