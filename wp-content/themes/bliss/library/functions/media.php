<?php
/***********************************************************
* Media functions
* Loads files and scripts for the theme
* @since 0.1
* See (get-the-image.php, get-the-video.php) for other media
***********************************************************/

/***********************************************************
* op_enqueue_style() - Displays stylesheets
* @since 0.1
* @hook wp_head()
***********************************************************/
function op_enqueue_style() {
	global $settings;

// Check whether to display Smooth Gallery CSS
	if($settings['print_style']) :
		wp_enqueue_style('print_css', OP_CSS . '/print.css', false, '0.1', 'print');
	endif;

// Print styles
	wp_print_styles(array('print_css'));
}

/***********************************************************
* op_enqueue_script() - Loads JavaScript
* Only load JS when it's needed
* Functions called should use wp_enqueue_script()
* @since 0.1
* @hook op_head()
***********************************************************/
function op_enqueue_script() {
	// Not JavaScript needed right now
	// Holder function for later
}
?>