msgid ""
msgstr ""
"Project-Id-Version: Critical Child Theme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-07-06 18:31-0600\n"
"PO-Revision-Date: 2010-07-06 18:31-0600\n"
"Last-Translator: Justin Tadlock <justin@justintadlock.com>\n"
"Language-Team: ThemeHybrid\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _e;__;esc_attr_e;esc_attr__;esc_html_e;esc_html__;_x;_ex;esc_attr_x;esc_html_x;_n;_nx;_n_noop;_nx_noop\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"

#: functions.php:60
msgid "Utility: Header"
msgstr ""

#: functions.php:102
msgid "No Responses"
msgstr ""

#: home.php:37
msgid "Sorry, no posts matched your criteria."
msgstr ""

