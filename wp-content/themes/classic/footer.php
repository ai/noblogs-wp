<?php
/**
 * @package WordPress
 * @subpackage Classic_Theme
 */
?>
<!-- begin footer -->
</div>

<?php get_sidebar(); ?>

<p class="credit"><!--<?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. --> <cite><?php echo sprintf(__("Powered by <a href='http://autistici.org/' title='%s'><strong>R*</strong></a>"), __("")); ?></cite></p>

</div>

<?php wp_footer(); ?>
</body>
</html>
