<?php get_header(); ?>

<?php
$this_category = get_category($cat);
$this_feed = get_category_feed_link($this_category->cat_ID,'');
?>

<div id="content">

<?php if (have_posts()) : ?>
<h1 class="cat-title"><a title="<?php _e('Feed for ', 'flipflop');single_cat_title();?>" href="<?php echo $this_feed;?>"><?php single_cat_title(); ?></a></h1>

<?php if(category_description()) :?>
<div class="cat-desc">
<?php echo category_description();?>
</div>
<?php endif;?>

<?php while (have_posts()) : the_post(); ?>

<div <?php post_class();?>>

<h2 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>

<ul class="meta">
<li><?php the_time(__('F j, Y', 'flipflop') ); ?> <?php the_time(); ?></li>
<li><?php edit_post_link(__('Edit', 'flipflop')); ?></li>
</ul>

<?php if( has_post_thumbnail() ) {
	the_post_thumbnail();
	the_excerpt();
}
else the_content(''); ?>

<ul class="meta postfoot">
<li><a href="<?php the_permalink();?>"><?php _e('Continue reading', 'flipflop');?> <?php the_title();?></a></li>
<?php if('open' == $post->comment_status) : ?><li><?php comments_popup_link(__('Comment on ', 'flipflop') .$post->post_title, __('1 Comment on ', 'flipflop') .$post->post_title, __('% Comments on ', 'flipflop') .$post->post_title,'postcomment',__('Comments are off for ', 'flipflop') .$post->post_title); ?></li><?php endif;?>
<?php if(get_the_tag_list()) :?>
<li><?php _e('Tags', 'flipflop');?>: <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li>
<?php endif;?>
</ul>

</div>
<?php endwhile; ?>

<ul class="prevnext">
<li class="next"><?php next_posts_link(__('Older Posts', 'flipflop') ); ?></li>
<li class="prev"><?php previous_posts_link(__('Newer Posts', 'flipflop') );?></li>
</ul>

<?php endif; ?>

<br class="clear-left" />

<!-- end content -->
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>