<?php 
// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	die ('Please do not load this page directly. Thanks!');
if ( post_password_required() ) {
	echo '<p class="nocomments">'._e('This post is password protected. Enter the password to view comments.','flipflop') .'</p>';
	return;
}
?>

<?php global $theme_options;?>

<?php if(have_comments()) : ?>
<h2 id="comments" class="total-comments"><?php comments_number(__('No comments', 'flipflop') , '1'.__(' Comment', 'flipflop') , '%'.__(' Comments', 'flipflop') ); ?> on <?php the_title(); ?></h2>
<?php endif;?>

<?php if ( 'open' == $post->comment_status && have_comments()) : ?>
<ul class="comment-links">
<li><a href="#respond"><?php _e('Add your comment', 'flipflop');?></a></li>
<?php if ( have_comments() ) : ?><li><a href="<?php echo get_post_comments_feed_link();?>"><?php _e('Comments feed for this post', 'flipflop'); ?></a></li><?php endif;?>
<?php if(pings_open()) : ?><li><a href="<?php trackback_url();?>" rel="trackback"><?php _e('TrackBack <abbr title="Uniform Resource Identifier">URI</abbr>', 'flipflop');?></a></li><?php endif;?>
</ul>

<?php elseif(pings_open()) : ?><p class="trackback"><a href="<?php trackback_url();?>" rel="trackback"><?php _e('TrackBack <abbr title="Uniform Resource Identifier">URI</abbr>', 'flipflop');?></a></p>

<?php endif;?>

<?php if(have_comments()) : ?>

<?php paginate_comments_links('type=list'); ?>

<ol id="commentlist">
<?php wp_list_comments(); ?>
</ol>

<?php paginate_comments_links('type=list'); ?>

<?php endif;?>

<?php if('open' == $post->comment_status) : ?>
<div id="respond">
<h3><?php comment_form_title(__( 'Leave a comment', 'flipflop') , __('Reply to %s', 'flipflop')  ); ?></h3>
<div class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></div>

<p class="comment_log_status">
<?php if (get_option('comment_registration') && !is_user_logged_in() ) : ?><?php _e('You must be', 'flipflop');?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('logged in', 'flipflop');?></a> <?php _e('to post a comment', 'flipflop');?>.
<?php elseif(is_user_logged_in() ) : ?><?php _e('You are currently logged in as', 'flipflop');?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a> - <a href="<?php echo wp_logout_url(get_permalink()); ?>"><?php _e('Log out', 'flipflop');?></a><?php endif;?>
</p>

<form action="<?php bloginfo('url'); ?>/wp-comments-post.php" method="post" id="commentform">
<fieldset>

<?php if(!is_user_logged_in()) : ?>
<p><label for="author" <?php if($req) echo 'class="req"';?>><?php _e('Name', 'flipflop');?> <?php if($req) echo '<small>('.__('required', 'flipflop') .')</small>';?></label> <input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="20" /></p>
<p><label for="email"<?php if($req) echo 'class="req"';?>><?php _e('Email', 'flipflop');?>  <?php if($req) echo '<small>('.__('required but will not be published', 'flipflop') .')</small>'; ?></label> <input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="40" /></p>
<p><label for="url"><?php _e('Website', 'flipflop');?></label> <input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="40" /></p>
<?php endif; ?>

<p><label for="comment" class="textarea"><span class="req"><?php _e('Comments', 'flipflop');?> <small>(<?php _e('required', 'flipflop');?>)</small></span>
<?php if( !$theme_options['hide_kses'] || $theme_options['hide_kses'] == 0) :?>
<br /><span class="kses"><?php _e('You can use the following', 'flipflop');?> <abbr title="eXtensible HyperText Markup Language">XHTML</abbr> <?php _e('tags:', 'flipflop');?> <code><?php echo allowed_tags(); ?></code></span><br />
<?php endif;?></label>
<textarea name="comment" id="comment" cols="100%" rows="10"></textarea></p>

<p class="submit_wrap"><input name="submit" type="submit" class="submit" value="<?php _e('Submit Comment', 'flipflop');?>" /> 
<?php do_action('comment_form', $post->ID); ?>
<?php comment_id_fields(); ?></p>
</fieldset>
</form>
</div>

<?php elseif(have_comments()) : ?>
<p class="comments-closed"><strong><?php _e('Sorry, the comment form is now closed.', 'flipflop');?></strong></p>
<?php endif;?>
