<?php
/*
Template Name: Widgetised Page
*/
?>
<?php get_header(); ?>
<div id="content">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class();?>>
<h2 class="post-title"><?php the_title(); ?></h2>
<ul class="meta">
<li><?php edit_post_link(__('Edit', 'flipflop')); ?></li>
</ul>

<?php the_content(); ?>

<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Widgetised Page')) : ?>
<br class="clear-left" />
<?php endif; ?>

<?php 
if(function_exists('theme_link_pages')) theme_link_pages(array('blink'=>'<li>','alink'=>'</li>','before' => '<div class="pagelist">'.__('Pages', 'flipflop').':<ul>', 'after' => '</ul></div>', 'next_or_number' => 'number'));
else wp_link_pages('before=<div class="pagelist">'.__('Pages', 'flipflop') .':&after=</div>&link_before=&link_after=&pagelink=%');
?>

</div>
<?php endwhile; endif; ?>

</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>