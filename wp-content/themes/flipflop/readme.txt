FlipFlop Wordpress Theme

INSTALLATION
Simply upload the flipflop theme folder to your wp-content themes directory .
Navigate to Admin/Appearance/Themes.
Select the FlipFlop theme.
Click "Activate".


CUSTOMISED META-KEYWORDS
The theme will attempt to automatically generate meta-keyords and meta-descriptions for all posts, pages and archives across your blog using a combination of WordPress titles, tags and descriptions. If you wish to create your own meta-keywords and meta-descriptions, on a post-by-post basis, use the Custom fields area of the Post/Page 
editing interface to set up 'description' and 'keywords' custom meta-fields.


CHANGING THE HEADER TEXT COLOR
Within the Admin area, navigate to Appearance / Custom Header Image.
Use the "Select A Text Color" to choose a new color for your header's text.
To activate your new color, select "Save Changes".
To reset the color back to the default, select "Use Original Color", then select "Save Changes".
All text color changes will take place immediately. Remember to press CTRL and F5 simultaneously when viewing the updated site. This should ensure that your web browser fetches a fresh copy of the page rather than serving up an out-dated copy from your own cache.


CHANGING THE HEADER IMAGE
Within the Admin area, navigate to Appearance / Custom Header Image.
Use the Browse button to locate your new header image on your computer.
Once you have selected your new image, click "Upload".
If your new image is larger than the dimensions indicated, you will then be given the opportunity to crop the image to the correct dimensions. Use your cursor to move the highlighted area to the section of the image that you would like to use and click once.

The final image will be saved to your wp-content/uploads folder and will be immediately incorporated into your site's header. 

If you wish to try another image, simply repeat this process.

If you wish to revert to the original image, navigate back to Appearance / Custom Header Image and select "Restore Original Header". 

All header changes will take place immediately. Remember to press CTRL and F5 simultaneously when viewing the updated site. This should ensure that your web browser fetches a fresh copy of the page rather than serving up an out-dated copy from your own cache.


THEME OPTIONS
The FlipFlop theme comes complete with a number of custom options:

- change the color of the theme
- change the header image
- change the logo image
- hide the logo image
- adjust the post excerpt length on post listing pages
- hide allowed tags for comments
- turn collapsing sub-menus on/off

The theme options will affect ALL Posts & Pages. They cannot be applied on a page-by-page basis.

All changes will take place immediately. Remember to press CTRL and F5 simultaneously when viewing the updated site. This should ensure that your web browser fetches a fresh copy of the page rather than serving up an out-dated copy from your own cache.

Theme Color
	Select your preferred color from those available.
	Click "Update Theme".

Display Logo
	Under "Display Logo" select "Yes" to display a logo in your site's header IN ADDITION to your header image.
	Select "No" to hide the logo image
	Click "Update Theme".

Logo Type
	Select "Custom Image" to use your own 120px by 120px logo image.

Custom Logo URL
	1. Navigate to Media / Add New
	2. Upload your custom logo image (120 x 120 pixels)
	3. Once the image has been uploaded, copy the web address in the File URL box.
	4. Navigate to Appearance / Theme Options.
	5. Ensure that the "Display Logo" is set to "Yes"
	6. Ensure that Logo Type is set to "Custom Image"
	7. Paste the address of your custom logo into the "Custom Logo URL" box
	8. Click "Update Theme".

Post Excerpt Length
	Enter the number of words you would like displayed in post teasers/excerpts followed by "Update Theme".

Allowed Comment tags
	To display all sub-menus in the theme's sidebar, select "Display".
	To hide the list of allowed markup tags in the comment form, select "Hide".
	Then click "Update Theme".

Collapsing sub-menus
	To collapse sub-menus in the theme's sidebar, select "Enable".
	To display all sub-menus in the theme's sidebar, select "Disable".
	Then click "Update Theme".

Reset Theme Options
	Ticking "Yes" followed by "Update Theme" will delete all previous theme options and the site will revert to displaying the default logo, header image and color.


WORDPRESS VIDEO TUTORIALS
http://wordpress.tv/


SUPPORT
Available via http://www.forum.quirm.net/