<?php
load_theme_textdomain ('flipflop', get_template_directory() . '/langs');
$GLOBALS['content_width'] = 1000;

remove_action ('wp_head', 'wp_generator');

// Constants
define('THEMELIB', get_template_directory() . '/library');
define('THEMELIB_URI', get_template_directory_uri() . '/library');
define('CUSTOM_THUMB', 150);

$theme_options = get_option('flipflop_options');

// Load common functions
require_once(THEMELIB . '/common.php');

// Load thickbox
require_once(THEMELIB . '/thickbox.php');

// Load custom header
require_once(THEMELIB . '/custom-header.php');

// Load custom theme options
require_once(THEMELIB . '/theme-options.php');

if (!$theme_options['accordian'] || $theme_options['accordian'] == 0) {
	// Load accordian menu
	require_once(THEMELIB . '/accordian.php');
}

// Custom post excerpt
function custom_excerpt_length() {
	global $theme_options;
	if( $theme_options['custom_excerpt'] && $theme_options['custom_excerpt'] != 0 ) return $theme_options['custom_excerpt'];
	else return 100; 
}
add_filter('excerpt_length', 'custom_excerpt_length');

if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'=> 'Top 1',
		'id' => 'nav',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
	register_sidebar(array(
		'name'=> 'Top 2',
		'id' => 'nav2',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
	register_sidebar(array(
		'name'=> 'Sidebar',
		'id' => 'sidebar',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
	register_sidebar(array(
		'name'=> 'Widgetised Page',
		'id' => 'page',
		'before_widget' => '<div id="%1$s" class="widget_wrapper"><div class="widget_box">',
		'after_widget' => '</div></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}

?>