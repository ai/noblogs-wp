<div class="sidebar accordian">
<ul>

<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar') ) : ?>

<li><ul>
<?php if( function_exists('theme_enhanced_cat_list')) {
	$curr_cat_id = get_query_var('cat');
	$catlist = wp_list_categories('title_li=&echo=0');
	theme_enhanced_cat_list($curr_cat_id, $catlist);
}
else wp_list_categories('title_li=');?>
</ul></li>

<li><h2><?php _e('Archives', 'flipflop');?></h2>
<ul><?php wp_get_archives(); ?></ul>
</li>

<li><ul>
<?php wp_list_bookmarks('title_li=&orderby=name&show_images=0&show_description=1');?>
</ul></li>

<?php endif; ?>

</ul>

</div>
