<?php get_header(); ?>
<div id="content">			

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class();?> id="post-<?php the_ID(); ?>">

<h2 class="post-title"><?php the_title(); ?></h2>
<ul class="meta">
<li><?php the_time(__('F j, Y', 'flipflop') ); ?> <?php the_time(); ?></li>
<li><?php edit_post_link(__('Edit', 'flipflop')); ?></li>
</ul>


<?php the_content(); ?>

<?php 
if(function_exists('theme_link_pages')) theme_link_pages(array('blink'=>'<li>','alink'=>'</li>','before' => '<div class="pagelist">'.__('Pages', 'flipflop').':<ul>', 'after' => '</ul></div>', 'next_or_number' => 'number'));
else wp_link_pages('before=<div class="pagelist">'.__('Pages', 'flipflop') .':&after=</div>&link_before=&link_after=&pagelink=%');
?>

<ul class="meta postfoot">
<li><?php _e('Filed under', 'flipflop');?>: <ul><li><?php the_category(',</li> <li>') ?></li></ul></li>
<?php if(get_the_tag_list()) :?>
<li><?php _e('Tags', 'flipflop');?>: <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li>
<?php endif;?>
</ul>

<ul id="extras">
<li class="email-friend"><a href="mailto:blank?body=<?php _e('I thought you might be interested in', 'flipflop');?> <?php the_title(); ?>. <?php _e('You can view it at', 'flipflop');?>: <?php the_permalink() ?>"><?php _e('Send to a friend', 'flipflop');?></a></li>
</ul>

<ul class="prevnext">
<li class="next"><?php next_post_link( '%link', '%title', false, ''); ?></li>
<li class="prev"><?php previous_post_link( '%link', '%title' ,false, '');?></li>
</ul>


<!--
<?php trackback_rdf(); ?>
-->

<?php comments_template();?>

<?php endwhile; else: ?>

<?php endif; ?>

</div>
</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>
