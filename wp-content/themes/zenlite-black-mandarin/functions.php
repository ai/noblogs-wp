<?php

add_filter( 'zenlite_background_color', 'black_mandarin_background_color' );
add_filter( 'zenlite_header_color', 'black_mandarin_header_color' );
function black_mandarin_background_color ($bgcolor) {
	$bgcolor= '000';
	return $bgcolor;
}
function black_mandarin_header_color ($color) {
	$color= '999';
	return $color;
}

function zenlite_admin_header_style() {
?>
<style type="text/css">
#headimg {
	height:150px;
	padding:0;
	margin:70px 0 0;
	background-color:#000;
 	background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/banner.jpg);
 	background-repeat:no-repeat;
 	background-position:top left;
	border-top:3px double #999;
	border-bottom:3px double #333;
}
#headimg h1,#headimg #desc {
	text-align:center;
	font-weight:normal;
	letter-spacing:.02em;
	position:relative;
	top:-70px;
}
#headimg h1 {
	margin:0;
	padding:0 15px;
	font-size:30px;
	line-height:1.2em;
	background-color:#000;
	color:#999;
}
#headimg #desc {
	display:block;
	margin:0;
	padding:0 0 13px;
	font-size:20px;
	background-color:#000;
	color:#707070;
}
#headimg a {
	text-decoration:none;
}
#headimg a:hover {
	text-decoration:underline;
}
</style>
<?php
}
