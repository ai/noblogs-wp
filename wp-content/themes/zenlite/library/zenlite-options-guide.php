<?php
/* ZENLITE OPTIONS GUIDE
*/
?>
<div id="zenlite-readme">

<h3><?php _e('Option Documentation', 'zenlite');?></h3>

<h4><?php _e('Display links to', 'zenlite');?></h4>
<?php _e('If you are not using widgets or a custom menu in your main navigation menu, you can choose to display links to either your Pages or your Categories.', 'zenlite');?>
<span class="default"><?php _e('Default: Pages', 'zenlite');?></span>

<h4><?php _e('Display site name &amp; tagline', 'zenlite');?></h4>
<?php _e("The theme will show your site's name and the tagline (as configured in Settings &rarr; General) above the header image. Leaving these fields blank will negatively impact on your searc engine optimisation (SEO). If you set this option to 'No', the site name and tagline will be hidden on graphical web browsers but will remain available to search engines.", 'zenlite');?>
<span class="default"><?php _e('Default: Yes', 'zenlite');?></span>

<h4><?php _e('Display "Pages in this section" list', 'zenlite');?></h4>
<?php _e("Pages that belong to a Parent/Child tree will automatically show a sidebox containing links to all Pages in the tree. Select 'No' to remove this box from all Pages.", 'zenlite');?>
<span class="default"><?php _e('Default: Yes', 'zenlite');?></span>

<h4><?php _e('Display auto-generated title on Posts/Pages without titles', 'zenlite');?></h4>
<?php _e("Titles in Pages and Posts are often used to create links within WordPress. If you forget to add a title to a document, this can cause problems in some situations. The theme will automatically generate a title ('No Title') on all such pages. If you do not want auto-generated titles, select 'No'. However, you may also see the Post Formats documentation for ways of hiding post titles on specific post formats.", 'zenlite');?>
<span class="default"><?php _e('Default: Yes', 'zenlite');?></span>

<h4><?php _e('Display author name/link on Posts', 'zenlite');?></h4>
<?php _e("Pretty much what it says on the box. Select 'No' if you do not want the author's name (and a link to thr author's profile and list of posts) to appear on Posts.", 'zenlite');?>
<span class="default"><?php _e('Default: Yes', 'zenlite');?></span>

<h4><?php _e('Display allowed tags on comment form', 'zenlite');?></h4>
<?php _e("The WordPress commenting system only allows a subset of markup tags to be used within comments. It can also display a list of the allowed tags below the comment box. Select 'No' if you do not want this list of allowed tags to be displayed.", 'zenlite');?>
<span class="default"><?php _e('Default: Yes', 'zenlite');?></span>

