<div class="wrap rhs">

<div class="box donate">
<h2><?php _e('Theme Donations', 'zenlite');?></h2>
<p><?php _e('Please support us so that we can support you.', 'zenlite');?></p>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" style="margin:0 auto; width:120px;">
<input type="hidden" name="cmd" value="_xclick" />
<input type="hidden" name="business" value="paypal@blackwidows.co.uk" />
<input type="hidden" name="item_name" value="ZenLite theme donation" />
<input type="hidden" name="buyer_credit_promo_code" value="" />
<input type="hidden" name="buyer_credit_product_category" value="" />
<input type="hidden" name="buyer_credit_shipping_method" value="" />
<input type="hidden" name="buyer_credit_user_address_change" value="" />
<input type="hidden" name="no_shipping" value="0" />
<input type="hidden" name="no_note" value="1" />
<input type="hidden" name="currency_code" value="GBP" />
<input type="hidden" name="tax" value="0" />
<input type="hidden" name="lc" value="GB" />
<input type="hidden" name="bn" value="PP-DonationsBF" />
<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but21.gif" name="submit" alt="Donate via paypal" />
<img src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1" alt="" />
</form>

<p class="thanks"><?php _e('Thank you!', 'zenlite');?></p>
</div>

<div class="box support">
<h2><?php _e('Theme Support', 'zenlite');?></h2>
<p><a href="http://forum.quirm.net">Quirm.net</a></p>
</div>

<div class="box child-themes">
<h2><?php _e('Child Themes', 'zenlite');?></h2>
<ul>
<li><a href="http://quirm.net/themes/zenlite-basic-child/">ZenLite Basic Child</a></li>
<li><a href="http://quirm.net/themes/zenlite/zenlite-black-mandarin/">ZenLite Black Mandarin</a></li>
<li><a href="http://quirm.net/themes/zenlite/zenlite-blue/">ZenLite Blue</a></li>
<li><a href="http://quirm.net/themes/zenlite/zenlite-citrus/">ZenLite Citrus</a></li>
<li><a href="http://quirm.net/themes/zenlite/zenlite-vertical/">ZenLite Vertical</a></li></ul>
</div>

</div>