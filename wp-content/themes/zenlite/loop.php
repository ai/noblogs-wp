<?php if( is_paged() ) :?>
<ul class="prevnext">
<li class="next"><?php next_posts_link(__('Older Posts', 'zenlite') ); ?></li>
<li class="prev"><?php previous_posts_link(__('Newer Posts', 'zenlite') );?></li>
</ul>
<?php endif;?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div <?php post_class();?> id="post-<?php the_ID();?>">

<?php
$format = get_post_format();
if ( false === $format ) $format = 'standard';
get_template_part( 'format', $format );
?>

</div>
<?php endwhile;?>

<ul class="prevnext">
<li class="next"><?php next_posts_link(__('Older Posts', 'zenlite') ); ?></li>
<li class="prev"><?php previous_posts_link(__('Newer Posts', 'zenlite') );?></li>
</ul>

<?php endif;