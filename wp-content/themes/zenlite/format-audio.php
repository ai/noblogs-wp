<h2 class="post-title"><?php if( !is_single() ) :?><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute( array('before' => __('Permalink to ', 'zenlite'), 'after' => '') ); ?>"><?php endif;?><?php the_title();?><?php if( !is_single() ) :?></a><?php endif;?></h2>

<?php if( !is_search() ) :?>
<ul class="meta">
<li><?php edit_post_link(); ?></li>
</ul>
<?php endif;?>

<?php if( is_single() ) : ?>
<div class="postcontent">
<?php if( is_search() ) the_excerpt();
else the_content();?>
</div>
<?php endif;?>

<?php if( !is_search() ) :?>
<ul class="meta postfoot">

<?php if( !is_single() ) :?><li class="more-link"><a href="<?php the_permalink();?>"  title="<?php the_title_attribute( array('before' => __('Permalink to ', 'zenlite'), 'after' => '') ); ?>"><?php if(has_post_thumbnail() ) : the_thumbnail(); else : ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/audio.png" width="100" height="100" alt="" /><?php endif;?> <?php printf( __('Listen to %1$s', 'zenlite'), get_the_title() );?></a></li><?php endif;?>

<li><?php _e('Posted on ', 'zenlite'); the_time(get_option('date_format')); the_time(get_option('time_format')); echo zenlite_author_display(__(' by ', 'zenlite'), '')?></li>

<?php if(!is_category() && get_the_category() ) :?><li class="cats"><?php _e('Filed under:', 'zenlite');?> <ul><li><?php the_category(',</li> <li>') ?></li></ul></li><?php endif;?>

<?php if( !is_tag() && get_the_tag_list() ) :?><li class="tags"><?php _e('Tags:', 'zenlite');?> <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li><?php endif;?>

</ul>
<?php endif;
comments_template();