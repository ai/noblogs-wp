<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/1">
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>" />

<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_get_archives('type=monthly&format=link'); ?>

<title><?php wp_title(':');?></title>

<?php wp_head(); ?>
</head>

<body id="top" <?php body_class(); ?>>

<div id="wrapper">

<ul class="jumplinks">
<li><a href="#content"><?php _e('Jump to Main Content', 'zenlite');?></a></li>
<li><a href="#footer"><?php _e('Jump to Footer', 'zenlite');?></a></li>
</ul>

<div id="header">
<h1><a href="<?php echo home_url( '/' ); ?>" title="<?php _e('Home', 'zenlite');?>"><?php bloginfo('name'); ?></a>
<small><?php bloginfo('description'); ?></small></h1>
</div>

<div id="header-image"></div>

<?php get_sidebar(); ?>

<div id="content">

