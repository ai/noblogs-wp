<div id="comments">
<div id="comment-title">
<?php if ( !empty($post->post_password) && $_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) : ?>
<p><?php _e('Enter your password to view comments.'); ?></p>
<?php return; endif; ?>

<h2><?php comments_number(__('No Comments'), __('1 Comment'), __('% Comments')); ?>
<?php if ( comments_open() ) : ?>
	<a href="#postcomment" onclick="jQuery('#comment')[0].focus(); return false;" title="<?php _e("Leave a comment"); ?>">&raquo;</a>
<?php endif; ?>
</h2>
<div class="rss" id="comment-rss"><?php post_comments_feed_link(__('<abbr title="Really Simple Syndication">RSS</abbr>')); ?>
<?php if ( pings_open() ) : ?>
	<a href="<?php trackback_url() ?>" rel="trackback"><?php _e('TrackBack <abbr title="Universal Resource Locator">URL</abbr>'); ?></a>
<?php endif; ?>
</div>
</div>

<div id="comment-list">
<?php if ( $comments ) : ?>
<?php $comment_number = 0?>
<?php foreach ($comments as $comment) : ?>
	<div class="comment-container <?php echo ++$comment_number % 2 ? "comment-odd" : "comment-even"; ?>">
        <h3 class="comment-number"><?php echo $comment_number;?></h3>
        <div id="comment-<?php comment_ID() ?>" class="comment">	
            <?php echo get_avatar( $comment, 48 ); ?>
            <div class="content"><?php comment_text() ?></div>
            <div class="meta"><cite><?php comment_type('<!--'.__('Comment').'-->', __('Trackback').' '.__('by').' ', __('Pingback').' '.__('by').' '); ?><?php comment_author_link() ?> @ <?php comment_date() ?> <?php comment_time() ?></cite> <?php edit_comment_link(__("Edit This"), ' | '); ?></div>
        </div>
    </div>

<?php endforeach; ?>
<?php else : // If there are no comments yet ?>
	<!--we already say 'no comments' in the title-->
<?php endif; ?>
</div>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="comment-form"><div id="comment-form-body">
<?php if ( comments_open() ) : ?>
<h2 id="postcomment"><?php _e('Leave a comment'); ?></h2>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p><?php printf(__('You must be <a href="%s">logged in</a> to post a comment.'), get_option('siteurl')."/wp-login.php?redirect_to=".urlencode(get_permalink()));?></p>
<?php else : ?>


<?php if ( $user_ID ) : ?>

<p><?php printf(__('Logged in as %s.'), '<a href="'.get_option('siteurl').'/wp-admin/profile.php">'.$user_identity.'</a>'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="<?php _e('Log out of this account') ?>"><?php _e('Log out &raquo;'); ?></a></p>

<?php else : ?>

<p><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" />
<label for="author"><small><?php _e('Name'); ?> <?php if ($req) _e('(required)'); ?></small></label></p>

<p><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" />
<label for="email"><small><?php _e('Mail (will not be published)');?> <?php if ($req) _e('(required)'); ?></small></label></p>

<p><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" />
<label for="url"><small><?php _e('Website'); ?></small></label></p>

<?php endif; ?>

<!--<p><small><strong>XHTML:</strong> <?php printf(__('You can use these tags: %s'), allowed_tags()); ?></small></p>-->
<div>
<textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4"></textarea><br />
<input name="submit" type="submit" id="submit" tabindex="5" value="<?php echo attribute_escape(__('Submit Comment')); ?>" />
</div>
<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />

<?php do_action('comment_form', $post->ID); ?>


<?php endif; // If registration required and not logged in ?>

<?php else : // Comments are closed ?>
<p><?php _e('Sorry, the comment form is closed at this time.'); ?></p>
<?php endif; ?>
</div>
<div id="comment-form-footer">
</div>
</form>
</div>