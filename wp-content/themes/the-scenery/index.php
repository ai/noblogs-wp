<?php
get_header();
?>

<?php
$previsou_day = null;
if (have_posts()) : while (have_posts()) : the_post();
$the_time = strtotime(get_the_time("Y-m-d H:i:s"));
$the_day = date('Y-M-j', $the_time);
if(!$previsou_day || $previsou_day != $the_day):
	$previsou_day = $the_day;?>
	<div class="date-container"><div class="date">
	<div class="year"><?php $the_year = date('Y', $the_time); if($the_year != date('Y')) echo $the_year; ?></div>
	<div class="month"><?php echo date('M', $the_time); ?>&nbsp;</div>
	<div class="day"><?php echo date('j', $the_time); ?></div>
	</div></div>
<?php endif;?>
<div class="post" id="post-<?php the_ID(); ?>">
	<h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<div class="meta"><?php
    if(!is_page() || $wp_query->post->post_parent){
        _e("Filed under:"); ?> 
    <?php
    }
    if(is_page()){
        if(empty($wp_query->post->post_parent)){
            ?><a href="<?php bloginfo('url'); ?>/"><?php bloginfo('name'); ?></a><?php
        }else{
            $parent_id = $wp_query->post->post_parent;
            wp_list_pages("include=$parent_id&title_li=");
        }
    }else{
        the_category(',');
    }
    ?> &#8212; <?php the_tags(__('Tags: '), ', ', ' &#8212; '); ?> <?php the_author() ?> @ <?php the_time() ?> <?php edit_post_link(__('Edit This')); ?>&nbsp;</div>

	<div class="content">
		<?php the_content(__('(more...)')); ?>
	</div>
        <div class="footer">
          <div class="feedback">
                  <?php wp_link_pages(); ?>
                  <?php comments_popup_link(__('Comments (0)'), __('Comments (1)'), __('Comments (%)')); ?>
          </div>
  
          <?php if (is_single()) :?>
          <div id="otherposts"><div id="previouspost"><?php previous_post_link(); ?></div>&nbsp;<div id="nextpost"><?php next_post_link(); ?></div></div>
          <?php endif;?>
        </div>
</div>
<?php comments_template(); // Get wp-comments.php template ?>

<?php endwhile; else: ?>
<div class="post"><p><?php _e('Sorry, no posts matched your criteria.'); ?></p></div>
<?php endif; ?>
<?php if (!is_single() && !is_page() && have_posts()) :?>
<div id="postpager">
  <div class="content"><?php posts_nav_link('', __('&laquo; Newer Posts'), __('Older Posts &raquo;')); ?></div>
  <div class="top"></div>
  <div class="bottom"></div>
</div>
<script type="text/javascript">jQuery("#postpager .content:empty").parents("#postpager").remove();</script>
<?php endif;?>
<?php get_footer(); ?>
