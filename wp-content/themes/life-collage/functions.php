<?php

/**
* This is your child theme's functions.php file.
* You should make edits and add additional code above this point.
* Only change the functions below if you know what you're doing.
*/

/********************************************************/

/* Load theme textdomain. */
load_child_theme_textdomain( 'life-collage', get_stylesheet_directory() . '/languages' );

/* Actions. */
add_action( 'init', 'life_collage_register_sidebars' );
add_action( 'template_redirect', 'life_collage_enqueue_style' );
add_action( 'hybrid_after_primary_menu', 'get_search_form' );
add_action( 'hybrid_before_container', 'life_collage_splash' );

/* Filters. */
add_filter( 'hybrid_entry_meta', 'life_collage_entry_meta' );
add_filter( 'hybrid_byline', 'life_collage_disable' );
add_filter( 'breadcrumb_trail', 'life_collage_disable' );
add_filter( 'sidebars_widgets', 'life_collage_remove_widgets' );
add_filter( 'gettext', 'life_collage_translations', 10, 3 );

/**
 * Registers widget areas for the theme.
 *
 * @since 0.3
 */
function life_collage_register_sidebars() {
	register_sidebar( array( 'name' => __( 'Utility: Splash', 'life-collage' ), 'id' => 'utility-splash', 'description' => __( 'Widget area that is placed in the Splash area of the site.', 'life-collage' ), 'before_widget' => '<div id="%1$s" class="widget %2$s widget-%2$s"><div class="widget-inside">', 'after_widget' => '</div></div>', 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>' ) );
	register_sidebar( array( 'name' => __( 'Utility: Lifestream Template', 'life-collage' ), 'id' => 'utility-lifestream', 'description' => __( 'Widget area used along with the Lifestream page template.', 'life-collage' ), 'before_widget' => '<div id="%1$s" class="widget %2$s widget-%2$s"><div class="widget-inside">', 'after_widget' => '</div></div>', 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>' ) );
}

/**
 * Loads the stylesheet for the Lifestream template.
 *
 * @since 0.2
 */
function life_collage_enqueue_style() {
	if ( is_page_template( 'page-lifestream.php' ) || is_page_template( 'lifestream.php' ) )
		wp_enqueue_style( 'page-lifestream', get_stylesheet_directory_uri() . '/page-lifestream.css', false, 0.2, 'screen' );
}

/**
 * Disables widget areas for the Lifestram page template.
 *
 * @since 0.2
 * @param array $sidebars_widgets
 */
function life_collage_remove_widgets( $sidebars_widgets ) {

	if ( is_page_template( 'page-lifestream.php' ) || is_page_template( 'lifestream.php' ) ) {
		$sidebars_widgets['primary'] = false;
		$sidebars_widgets['secondary'] = false;
	}

	return $sidebars_widgets;
}

/**
 * Loads the sidebar-splash.php template.
 *
 * @since 0.2
 */
function life_collage_splash() {
	get_sidebar( 'splash' );
}

/**
 * Filters the entry metadata of posts.  Since we're not using a byline with this theme, we need
 * to add the 'Edit' link to the metadata section.
 *
 * @since 0.2
 * @param string $meta Metadata passed through the hybrid_entry_meta filter hook.
 */
function life_collage_entry_meta( $meta ) {

	if ( 'post' == get_post_type() )
		$meta = str_replace( '</p>', ' [entry-edit-link before=" | "]</p>', $meta );

	return $meta;
}

/**
 * Returns false for specific hooks.
 *
 * @since 0.2
 * @param mixed $var Original argument from filter hook.
 */
function life_collage_disable( $var ) {
	return false;
}

/**
 * Translations specific to the Life Collage child theme.
 *
 * @since 0.2
 * @param string $translation Translated text.
 * @param string $text Original text.
 * @param string $domain Textdomain of the text.
 */
function life_collage_translations( $translation, $text, $domain ) {

	if ( 'hybrid' == $domain ) {
		$translations = &get_translations_for_domain( $domain );

		if ( '&laquo; Previous' == $text )
			$translation = $translations->translate( __( 'Previous', 'life-collage' ) );
		elseif ( 'Next &raquo;' == $text )
			$translation = $translations->translate( __( 'Next', 'life-collage' ) );
	}

	return $translation;
}

?>