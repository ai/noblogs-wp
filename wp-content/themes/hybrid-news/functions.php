<?php
/**
 * This is your child theme functions file.  In general, most PHP customizations should be placed within this
 * file.  Sometimes, you may have to overwrite a template file.  However, you should consult the theme 
 * documentation and support forums before making a decision.  In most cases, what you want to accomplish
 * can be done from this file alone.  This isn't a foreign practice introduced by parent/child themes.  This is
 * how WordPress works.  By utilizing the functions.php file, you are both future-proofing your site and using
 * a general best practice for coding.
 *
 * All style/design changes should take place within your style.css file, not this one.
 *
 * The functions file can be your best friend or your worst enemy.  Always double-check your code to make
 * sure that you close everything that you open and that it works before uploading it to a live site.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume 
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write 
 * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * @package HybridNews
 * @subpackage Functions
 * @version 0.4.0
 * @author Justin Tadlock <justin@justintadlock.com>
 * @copyright Copyright (c) 2008 - 2011, Justin Tadlock
 * @link http://themehybrid.com/themes/hybrid-news
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* Set up the Hybrid News child theme and its default functionality. */
add_action( 'after_setup_theme', 'hybrid_news_setup', 11 );

/**
 * Adds all the default actions and filters to their appropriate hooks and sets up anything
 * else needed by the theme.
 *
 * @since 0.3.0
 */
function hybrid_news_setup() {

	/* Get the parent theme prefix for use with its hooks. */
	$prefix = hybrid_get_prefix();

	/* Load any translation files for the user. */
	load_child_theme_textdomain( 'hybrid-news', get_stylesheet_directory() );

	/* Add support for the Cleaner Gallery extension. */
	add_theme_support( 'cleaner-gallery' );

	/* Register additional menus. */
	add_action( 'init', 'hybrid_news_register_menus', 11 );

	/* Register additional sidebars. */
	add_action( 'init', 'hybrid_news_register_sidebars', 11 );

	/* Perform specific functions for the front page template. */
	add_action( 'template_redirect', 'hybrid_news_front_page_template' );

	/* Add the secondary menu before the header. */
	add_action( "{$prefix}_before_header", 'hybrid_news_get_secondary_menu' );

	/* Add the header sidebar to the header. */
	add_action( "{$prefix}_header", 'hybrid_news_get_utility_header', 11 );

	/* Add the post author box after singular posts. */
	add_action( "{$prefix}_singular-post_after_singular", 'hybrid_news_author_box' );

	/* Add the tertiary sidebar after the secondary sidebar. */
	add_action( "{$prefix}_after_container", 'hybrid_news_get_tertiary', 11 );

	/* Set up the theme settings meta box. */
	add_action( 'admin_menu', 'hybrid_news_create_meta_box' );

	/* Save the theme settings meta box settings. */
	add_filter( "sanitize_option_{$prefix}_theme_settings", 'hybrid_news_save_meta_box' );
}

/**
 * Registers additional nav menus for use with this theme.
 *
 * @since 0.3.0
 */
function hybrid_news_register_menus() {
	register_nav_menu( 'secondary-menu', __( 'Secondary Menu', 'hybrid-news' ) );
}

/**
 * Loads the menu-secondary.php template, which displays the Secondary Menu.
 *
 * @since 0.3.0
 */
function hybrid_news_get_secondary_menu() {
	locate_template( array( 'menu-secondary.php', 'menu.php' ), true );
}

/**
 * Register additional widget areas
 *
 * @since 0.3.0
 */
function hybrid_news_register_sidebars() {
	register_sidebar( array( 'name' => __( 'Tertiary', 'hybrid-news' ), 'id' => 'tertiary', 'before_widget' => '<div id="%1$s" class="widget %2$s widget-%2$s"><div class="widget-wrap widget-inside">', 'after_widget' => '</div></div>', 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>' ) );
	register_sidebar( array( 'name' => __( 'Utility: Header', 'hybrid-news' ), 'id' => 'utilityheader', 'before_widget' => '<div id="%1$s" class="widget %2$s widget-%2$s"><div class="widget-wrap widget-inside">', 'after_widget' => '</div></div>', 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>' ) );
}

/**
 * Loads the sidebar-header.php template, which loads the Utility: Header sidebar.
 *
 * @since 0.3.0
 */
function hybrid_news_get_utility_header() {
	get_sidebar( 'header' );
}

/**
 * Loads the sidebar-tertiary.php template, which displays the Tertiary sidebar.
 *
 * @since 0.3.0
 */
function hybrid_news_get_tertiary() {
	get_sidebar( 'tertiary' );
}

/**
 * Adds JavaScript and CSS to Front Page page template.
 * Also removes the breadcrumb menu.
 *
 * @since 0.3.0
 */
function hybrid_news_front_page_template() {

	/* If we're not looking at the front page template, return. */
	if ( !is_page_template( 'page-front-page.php' ) )
		return;

	/* Load the jQuery Cycle plugin JavaScript and custom JavaScript for it. */
	wp_enqueue_script( 'slider', get_stylesheet_directory_uri() . '/js/jquery.cycle.js', array( 'jquery' ), 0.1, true );

	/* Load the front page stylesheet. */
	wp_enqueue_style( 'front-page', get_stylesheet_directory_uri() . '/css/front-page.css', false, '0.1', 'screen' );

	/* Remove the breadcrumb trail. */
	add_filter( 'breadcrumb_trail', '__return_false' );
}

/**
 * Shows an author description after the post but before the comments section on
 * singular post views.
 *
 * @since 0.3.0
 */
function hybrid_news_author_box() { ?>
	<div class="author-profile vcard">
		<?php echo get_avatar( get_the_author_meta( 'email' ), '96' ); ?>
		<h4 class="author-name fn n"><?php the_author_posts_link(); ?></h4>
		<?php echo wpautop( get_the_author_meta( 'description' ) ); ?>
	</div><?php
}

/**
 * Saves the theme settings for Hybrid News if the user has added any.
 *
 * @since 0.3.0
 */
function hybrid_news_save_meta_box( $settings ) {

	$settings['feature_category'] = empty( $settings['feature_category'] ) ? '' : absint( $settings['feature_category'] );
	$settings['excerpt_category'] = empty( $settings['excerpt_category'] ) ? '' : absint( $settings['excerpt_category'] );
	$settings['feature_num_posts'] = empty( $settings['feature_num_posts'] ) ? 3 : absint( $settings['feature_num_posts'] );
	$settings['excerpt_num_posts'] = empty( $settings['excerpt_num_posts'] ) ? 3 : absint( $settings['excerpt_num_posts'] );
	$settings['headlines_num_posts'] = empty( $settings['headlines_num_posts'] ) ? 5 : absint( $settings['headlines_num_posts'] );
	$settings['headlines_category'] = empty( $settings['headlines_category'] ) || !is_array( $settings['headlines_category'] ) ? '' : $settings['headlines_category'];

	return $settings;
}

/**
 * Adds a meta box to the theme settings page in the admin.
 *
 * @since 0.3.0
 */
function hybrid_news_create_meta_box() {
	add_meta_box( 'hybrid-news-front-page-box', __( 'Front Page template settings', 'hybrid-news' ), 'hybrid_news_front_page_meta_box', 'appearance_page_theme-settings', 'normal', 'low' );
}

/**
 * Outputs the meta box and its form for the theme settings page.
 *
 * @since 0.3.0
 */
function hybrid_news_front_page_meta_box() {
	$categories = get_categories(); ?>

	<table class="form-table">

		<tr>
			<th><label for="<?php echo hybrid_settings_field_id( 'feature_category' ); ?>"><?php _e( 'Feature Category:', 'hybrid-news' ); ?></label></th>
			<td>
				<select id="<?php echo hybrid_settings_field_id( 'feature_category' ); ?>" name="<?php echo hybrid_settings_field_name( 'feature_category' ); ?>">
					<option value="" <?php selected( hybrid_get_setting( 'feature_category' ), '' ); ?>></option>
				<?php foreach ( $categories as $cat ) { ?>
					<option value="<?php echo $cat->term_id; ?>" <?php selected( hybrid_get_setting( 'feature_category' ), $cat->term_id ); ?>><?php echo esc_html( $cat->name ); ?></option>
				<?php } ?>
				</select> 
				<?php _e( 'Leave blank to use sticky posts.', 'hybrid-news' ); ?>
			</td>
		</tr>
		<tr>
			<th><label for="<?php echo hybrid_settings_field_id( 'feature_num_posts' ); ?>"><?php _e( 'Featured Posts:', 'hybrid-news' ); ?></label></th>
			<td>
				<input type="text" id="<?php echo hybrid_settings_field_id( 'feature_num_posts' ); ?>" name="<?php echo hybrid_settings_field_name( 'feature_num_posts' ); ?>" value="<?php echo esc_attr( hybrid_get_setting( 'feature_num_posts' ) ); ?>" size="2" maxlength="2" />
				<label for="<?php echo hybrid_settings_field_id( 'feature_num_posts' ); ?>"><?php _e( 'How many feature posts should be shown?', 'hybrid-news' ); ?></label>
			</td>
		</tr>
		<tr>
			<th><label for="<?php echo hybrid_settings_field_id( 'excerpt_category' ); ?>"><?php _e( 'Excerpts Category:', 'hybrid-news' ); ?></label></th>
			<td>
				<select id="<?php echo hybrid_settings_field_id( 'excerpt_category' ); ?>" name="<?php echo hybrid_settings_field_name( 'excerpt_category' ); ?>">
					<option value="" <?php selected( hybrid_get_setting( 'excerpt_category' ), '' ); ?>></option>
					<?php foreach( $categories as $cat ) { ?>
						<option value="<?php echo $cat->term_id; ?>" <?php selected( hybrid_get_setting( 'excerpt_category' ), $cat->term_id ); ?>><?php echo esc_html( $cat->name ); ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="<?php echo hybrid_settings_field_id( 'excerpt_num_posts' ); ?>"><?php _e( 'Excerpts Posts:', 'hybrid-news' ); ?></label></th>
			<td>
				<input type="text" id="<?php echo hybrid_settings_field_id( 'excerpt_num_posts' ); ?>" name="<?php echo hybrid_settings_field_name( 'excerpt_num_posts' ); ?>" value="<?php echo esc_attr( hybrid_get_setting( 'excerpt_num_posts' ) ); ?>" size="2" maxlength="2" />
				<label for="<?php echo hybrid_settings_field_id( 'excerpt_num_posts' ); ?>"><?php _e('How many excerpts should be shown?', 'hybrid-news' ); ?></label>
			</td>
		</tr>
		<tr>
			<th><label for="<?php echo hybrid_settings_field_id( 'headlines_category' ); ?>"><?php _e( 'Headline Categories:', 'hybrid-news' ); ?></label></th>
			<td>
				<label for="<?php echo hybrid_settings_field_id( 'headlines_category' ); ?>"><?php _e( 'Multiple categories may be chosen by holding the <code>Ctrl</code> key and selecting.', 'hybrid-news' ); ?></label>
				<br />
				<select id="<?php echo hybrid_settings_field_id( 'headlines_category' ); ?>" name="<?php echo hybrid_settings_field_name( 'headlines_category' ); ?>[]" multiple="multiple" style="height:150px;">
				<?php foreach( $categories as $cat ) { ?>
					<option value="<?php echo $cat->term_id; ?>" <?php if ( is_array( hybrid_get_setting( 'headlines_category' ) ) && in_array( $cat->term_id, hybrid_get_setting( 'headlines_category' ) ) ) echo ' selected="selected"'; ?>><?php echo esc_html( $cat->name ); ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="<?php echo hybrid_settings_field_id( 'headlines_num_posts' ); ?>"><?php _e('Headlines Posts:', 'hybrid-news' ); ?></label></th>
			<td>
				<input type="text" id="<?php echo hybrid_settings_field_id( 'headlines_num_posts' ); ?>" name="<?php echo hybrid_settings_field_name( 'headlines_num_posts' ); ?>" value="<?php echo esc_attr( hybrid_get_setting( 'headlines_num_posts' ) ); ?>" size="2" maxlength="2" />
				<label for="<?php echo hybrid_settings_field_id( 'headlines_num_posts' ); ?>"><?php _e( 'How many posts should be shown per headline category?', 'hybrid-news' ); ?></label>
			</td>
		</tr>

	</table><!-- .form-table --><?php
}

?>