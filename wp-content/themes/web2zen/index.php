<?php get_header(); ?>

<?php
add_filter('excerpt_length', 'index_excerpt_length');
function index_excerpt_length($length) {
	return 35;
}
$c = 0;
?>

<div id="content" class="<?php echo content_class();?>">

<?php $categories = get_categories(  ); 
echo '<pre>';
print_r($categories);
echo '</pre>';
?> 

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php if( is_sticky() && !is_paged() ) {
	$featured='featured';
	$c++;
	if( $c % 2 != 0 ) $featured .=' fleft';
}
else $featured = '';?>

<div <?php post_class($featured);?> id="post-<?php the_ID();?>">

<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?> - <?php _e('continue reading', 'web2zen');?>"><?php the_title(); ?></a></h2>

<?php if( !is_sticky() && !is_paged() ) :?>
<ul class="meta">
<li><?php the_time("F j, Y"); ?> <?php if($theme_options['post_time'] != 1) :?><?php the_time(); ?><?php endif;?></li>
<li><?php edit_post_link(__('Edit','web2zen') , '', ''); ?></li>
</ul>
<?php endif;?>

<div class="postcontent">
<?php if( !is_sticky() || is_paged() ) the_content(__('Continue reading ','web2zen')  . the_title('', '', false).'');
else {
	if( has_post_thumbnail() ) : the_post_thumbnail('featured');
	else :?><img height="100" width="100" alt="" class="attachment-featured wp-post-image" src="<?php bloginfo('template_directory');?>/images/default.jpg" /><?php endif;
	the_excerpt();
}
?> 
</div>

<?php if( !is_sticky() || is_paged() ) :?>
<?php 
if(function_exists('theme_link_pages')) theme_link_pages(array('blink'=>'<li>','alink'=>'</li>','before' => '<div class="pagelist">'.__('Pages','web2zen').':<ul>', 'after' => '</ul></div>', 'next_or_number' => 'number'));
else wp_link_pages('before=<div class="pagelist">'.__('Pages','web2zen') .':&after=</div>&link_before=&link_after=&pagelink=%');
?>

<ul class="meta postfoot">
<?php if('open' == $post->comment_status) : ?><li class="comment_link"><?php comments_popup_link(__('Comment on ','web2zen') .$post->post_title, __('1 Comment on ','web2zen') .$post->post_title, __('% Comments on ','web2zen') .$post->post_title,'postcomment',__('Comments are off for ','web2zen') .$post->post_title); ?></li><?php endif;?>
<?php if($theme_options['author_link'] != 1) :?><li><?php _e('Author','web2zen');?>: <?php the_author_posts_link(); ?></li><?php endif;?>
<li><?php _e('Filed under','web2zen');?>: <ul><li><?php the_category(',</li> <li>') ?></li></ul></li>
<?php if(get_the_tag_list()) :?>
<li><?php _e('Tags','web2zen');?>: <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li>
<?php endif;?>
</ul>

<?php else:?>
<?php endif;?>

</div>

<?php endwhile; ?>

<ul class="prevnext">
<li class="next"><?php next_posts_link(__('Older Posts','web2zen') ); ?></li>
<li class="prev"><?php previous_posts_link(__('Newer Posts','web2zen') );?></li>
</ul>

<?php endif; ?>

<?php remove_filter('excerpt_length', 'index_excerpt_length');?>

<?php get_footer(); ?>
