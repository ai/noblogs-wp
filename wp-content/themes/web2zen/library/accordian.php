<?php

function init_accordian() {
    wp_enqueue_script('accordian',
    get_bloginfo('template_directory') . '/library/accordian-menu.js',
    array('jquery'),
    '3.0.2'
    );            
}    
if( !is_admin() ) add_action('init', 'init_accordian');

?>