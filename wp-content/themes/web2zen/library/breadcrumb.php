<?php
// Web2Zenbreadcrumb trail

function theme_breadcrumb($value) {
	$output = '<div class="breadcrumb">' . __('You are viewing', 'web2zen') . ': <ul>'."\n";
	if( is_page() ) $output .= theme_page_crumb($value);
	elseif( is_attachment() ) $output .= theme_attachment_crumb($value);
	elseif( is_single() ) $output .= theme_single_crumb($value);
	elseif( is_category() ) $output .= theme_cat_crumb($value);
	else {
		$output .= '<li><a href="' . get_bloginfo('url') .'/">' . get_bloginfo('name') . '</a></li>'."\n";
		if( is_author() ) {
			$output .= '<li> &raquo; <a href="' . get_bloginfo('url') . '/authors/">'.__('Authors','web2zen') .'</a></li>';
			$output .= '<li> &raquo; ' . $value . '</li>';
		}
		elseif( is_tag() ) $output .= '<li> &raquo; ' . __('All posts tagged ','web2zen') . ' <span class="tag-title">' . single_tag_title('', false) . '</span></li>';
		elseif( is_404() ) $output .= '<li> &raquo; ' . __('Not Found ','web2zen'). '</li>';
		else $output .= '<li> &raquo; '.__('Archives for ','web2zen') .$value.'</li>';
	}
	$output .= "\n</ul></div>\n";
	return $output;
}
function theme_cat_crumb($c) {
	$clist = '<li><a href="' . get_bloginfo('url') . '/">' . get_bloginfo('name') . '</a></li>'."\n";
	$catlist = explode('|',$c);
	$empty = array_pop($catlist);
	if(!is_single()) {
		$this_cat = array_pop($catlist);
		array_push($catlist,strip_tags($this_cat));
	}
	foreach($catlist as $item) {
		$clist .= '<li> &raquo; '.$item."</li>\n";
	}
	return $clist;
}
function theme_single_crumb($id) { // single post breadcrumb
	$category = get_the_category($id);
	$this_cat_id = get_category_parents($category[0]->cat_ID, TRUE, '|');
	$slist = theme_cat_crumb($this_cat_id);
	$slist .= '<li> &raquo; '.the_title('','',false).'</li>';
	return $slist;
}
function theme_page_crumb($id) { // page breadcrumb
	$plist = '<li><a href="' . get_bloginfo('url') . '/">' . get_bloginfo('name') . '</a></li>'."\n";
	$breadcrumb = array();
	$this_page = get_post($id);
	$parent_id = $this_page->post_parent;
	while ($parent_id) {
		$ancestor = get_page($parent_id);
		$breadcrumb[] = '<li> &raquo; <a href="'.get_permalink($ancestor->ID).'" title="">'.get_the_title($ancestor->ID).'</a>';
		$parent_id  = $ancestor->post_parent;
	}
	$breadcrumb = array_reverse($breadcrumb);
	foreach ($breadcrumb as $crumb) $plist .= $crumb."</li>\n";
	$plist .= '<li> &raquo; '.$this_page->post_title.'</li>';
	return $plist;
}
function theme_attachment_crumb($id) { // attachment breadcrumb
	$category = get_the_category($id);
	$this_cat_id = get_category_parents($category[0]->cat_ID, TRUE, '|');
	$ilist = theme_cat_crumb($this_cat_id);
	$parent = get_post_ancestors($id);
	$ilist .= '<li> &raquo; <a href="' . get_permalink($parent[0]) . '" rev="attachment">' . get_the_title($parent[0]) . '</a></li>';
	$ilist .= '<li> &raquo; '.the_title('','',false).'</li>';
	return $ilist;
}

?>
