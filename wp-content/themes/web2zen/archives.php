<?php
/*
Template Name: Archives
*/
?>
<?php get_header(); ?>

<div id="content" class="<?php echo content_class();?>">

<?php if (function_exists('theme_breadcrumb') && $theme_options['breadcrumb'] != 1) echo theme_breadcrumb($post->ID);?>

<div <?php post_class(); ?>>
<h2 class="post-title"><?php _e('Archives by Month','web2zen');?></h2>
<ul class="archives">
<?php wp_get_archives('type=monthly'); ?>
</ul>

<h2 class="post-title"><?php _e('Archives by Subject','web2zen');?></h2>
<ul class="archives">
<?php wp_list_categories('title_li='); ?>
</ul>
</div>

<?php get_footer(); ?>