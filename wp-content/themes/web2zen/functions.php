<?php
load_theme_textdomain ('purplepastels', get_template_directory() . '/langs');
$GLOBALS['content_width'] = 800;

remove_action ('wp_head', 'wp_generator');

// Constants
define('THEMELIB', get_template_directory() . '/library');
define('THEMELIB_URI', get_template_directory_uri() . '/library');
define('SSLIB', get_stylesheet_directory() . '/library');
define('SSLIB_URI', get_stylesheet_directory_uri() . '/library');

$theme_options = get_option('web2zen_options');

// Load common functions
require_once(THEMELIB . '/common.php');

// Load thickbox
require_once(THEMELIB . '/thickbox.php');

// Load accordian menu
require_once(THEMELIB . '/accordian.php');

// Load breadcrumb
require_once(THEMELIB . '/breadcrumb.php');

// Load custom header
require_once(THEMELIB . '/custom-header.php');

// Load custom header
require_once(THEMELIB . '/theme-options.php');

// Set up custom post image sizes
if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'featured', 100, 100 ); 
}

// Add content class
function content_class() {
	global $theme_options;
	if($theme_options['sidebar'] != 1) $class='narrow'; 
	else $class = 'wide';
	return $class;
}

// Register sidebar widgets
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'=> 'Top Tabs',
		'id' => 'top_tabs',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
	));
	register_sidebar(array(
		'name'=> 'Right Sidebar',
		'id' => 'right_sidebar',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
	));
}

?>