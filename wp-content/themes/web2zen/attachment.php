<?php get_header(); ?>

<div id="content" class="<?php echo content_class();?>">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class();?> id="post-<?php the_ID();?>">
<h2 class="post-title"><a href="<?php echo get_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link','web2zen');?>: <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

<p><?php echo theme_attachment_download_link(__('This file requires ','web2zen'),  $post->post_mime_type, __('or','web2zen') );?></p>

<div class="attachment">
<?php echo theme_attachment_link($post->ID); ?>
</div>

<div class="attachment_description">
<?php the_content();?>
</div>

<p><?php _e('Posted under','web2zen');?> <a href="<?php echo get_permalink($post->post_parent); ?>" rev="attachment"><?php echo get_the_title($post->post_parent); ?></a></p>

<?php endwhile; ?><?php endif; ?>
<!-- end post -->
</div>
 
<?php get_footer(); ?>