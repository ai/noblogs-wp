<div class="sidebar accordian" id="right_sidebar">

<ul>
<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Right Sidebar')) : ?>
<li><?php get_search_form();?></li>

<li><h2><?php _e('Categories','web2zen');?></h2>
<ul>
<?php if( function_exists('theme_enhanced_cat_list')) {
	$curr_cat_id = get_query_var('cat');
	$catlist = wp_list_categories('title_li=&echo=0');
	theme_enhanced_cat_list($curr_cat_id, $catlist);
}
else wp_list_categories('title_li=');?>
</ul>
</li>

<li><h2><?php _e('Archives','web2zen');?></h2><ul>
<?php wp_get_archives('type=monthly'); ?>
</ul></li>

<?php endif; ?>
</ul>

</div>
