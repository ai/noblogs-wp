<?php get_header(); ?>

<div id="content" class="<?php echo content_class();?>">

<?php if (function_exists('theme_breadcrumb') && $theme_options['breadcrumb'] != 1 && !is_front_page()) echo theme_breadcrumb($post->ID);?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class(); ?>>

<h2 class="post-title" id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link', 'web2zen');?>: <?php the_title(); ?>"><?php the_title(); ?></a></h2>
<ul class="meta">
<li><?php edit_post_link(__('Edit','web2zen') ,'<span class="edit">','</span>'); ?></li>
</ul>

<?php if (function_exists('theme_page_tree') && theme_page_tree($post)) :?>
<div class="subpages">
<h3><?php _e('Pages in this section', 'web2zen');?></h3>
<ul>
<?php echo theme_page_tree($post);?>
</ul></div>
<?php endif;?>

<div class="postcontent">
<?php the_content(); ?>
</div>

<?php 
if(function_exists('theme_link_pages')) theme_link_pages(array('blink'=>'<li>','alink'=>'</li>','before' => '<div class="pagelist">'.__('Pages','web2zen').':<ul>', 'after' => '</ul></div>', 'next_or_number' => 'number'));
else wp_link_pages('before=<div class="pagelist">'.__('Pages','web2zen') .':&after=</div>&link_before=&link_after=&pagelink=%');
?>
</div>

<?php comments_template();?>

<?php endwhile; ?>

<?php  endif; ?>

<?php get_footer(); ?>