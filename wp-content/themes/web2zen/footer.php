<div class="clear"></div>
<!-- end content -->
</div>

<?php global $theme_options;
if($theme_options['sidebar'] != 1) get_sidebar('right'); ?>

<div id="footer">
<p class="rss"><a href="<?php bloginfo('rss2_url'); ?>" title="RSS <?php _e('Feed','web2zen');?>"><span>RSS</span></a></p>

<ul>
<li><a href="#top" title="<?php _e('Jump to the top of this page','web2zen');?>"><?php _e('Top','web2zen');?></a></li>
<li><?php wp_loginout(); ?></li>
<?php wp_register(); ?>
</ul>

<?php $theme_data = get_theme_data(get_stylesheet_directory() . '/style.css');?>
<p><?php _e('Powered by the', 'web2zen');?> <a href="<?php echo $theme_data['URI'];?>"><?php echo get_current_theme();?> <?php _e('Theme','web2zen');?></a></p>
<?php wp_footer(); ?>
</div>

<!-- end wrapper -->
</div>

</body>
</html>

