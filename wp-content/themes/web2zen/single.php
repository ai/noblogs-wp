<?php get_header(); ?>

<div id="content" class="<?php echo content_class();?>">

<?php if (function_exists('theme_breadcrumb') && $theme_options['breadcrumb'] != 1) echo theme_breadcrumb($post->ID);?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php if(is_sticky()) post_class('single sticky');else post_class('single');?>id="post-<?php the_ID();?>">

<h2 class="post-title"><?php the_title(); ?></h2>

<ul class="meta">
<li><?php the_time('F j, Y'); ?> <?php if($theme_options['post_time'] != 1) :?><?php the_time(); ?><?php endif;?></li>
<li><?php edit_post_link(__('Edit','web2zen') , '', ''); ?></li>
</ul>

<div class="postcontent">
<?php the_content(); ?>
</div>

<?php 
if(function_exists('theme_link_pages')) theme_link_pages(array('blink'=>'<li>','alink'=>'</li>','before' => '<div class="pagelist">'.__('Pages','web2zen').':<ul>', 'after' => '</ul></div>', 'next_or_number' => 'number'));
else wp_link_pages('before=<div class="pagelist">'.__('Pages','web2zen') .':&after=</div>&link_before=&link_after=&pagelink=%');
?>

<ul class="meta postfoot">
<?php if($theme_options['author_link'] != 1) :?><li><?php _e('Author','web2zen');?>: <?php the_author_posts_link(); ?></li><?php endif;?>
<li><?php _e('Filed under','web2zen');?>: <ul><li><?php the_category(',</li> <li>') ?></li></ul></li>
<?php if(get_the_tag_list()) :?>
<li><?php _e('Tags','web2zen');?>: <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li>
<?php endif;?>
</ul>

<!--
<?php trackback_rdf(); ?>
-->

</div>

<ul class="prevnext">
<li class="next"><?php next_post_link( '%link', '%title', false, ''); ?></li>
<li class="prev"><?php previous_post_link( '%link', '%title' ,false, '');?></li>
</ul>

<?php comments_template();?>

<?php endwhile; ?>

<?php  endif; ?>

<?php get_footer(); ?>
