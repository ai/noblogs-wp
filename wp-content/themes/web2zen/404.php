<?php ob_start(); ?>
<?php header("HTTP/1.1 404 Not Found"); ?>
<?php header("Status: 404 Not Found"); ?>
<?php get_header(); ?>

<div id="content" class="<?php echo content_class();?>">

<div <?php post_class('page'); ?>>
<h2 class="post-title"><?php _e('Page Not Found','web2zen');?></h2>

<p><?php _e('Uh oh! I cannot seem to find the file you asked for','web2zen');?>.</p>

<p><?php _e('Perhaps you','web2zen');?>:</p>

<ul>
<li><?php _e('tried to access a post or archive which has been removed','web2zen');?></li>
<li><?php _e('followed a bad link','web2zen');?></li>
<li><?php _e('mis-typed something','web2zen');?></li>
</ul>

<p><?php _e('Try using the main navigation menu to find what you are looking for','web2zen');?>.</p>

<p><?php _e('Or you may just wish to return to the','web2zen');?> <a href="<?php bloginfo('url'); ?>/"><?php _e('Home page','web2zen');?></a>.</p>

</div>

<?php get_footer(); ?>