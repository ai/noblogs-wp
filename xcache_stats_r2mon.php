<?php

$EXPORTED_STATS = array(
  "compiling", "disabled", "misses", "hits", "clogs",
  "ooms", "errors", "cached", "deleted", "size", "avail"
);

if (!extension_loaded('XCache')) {
  trigger_error("xcache not loaded", E_USER_ERROR);
  header("HTTP/1.1 500 Internal Server Error");
}

function xcache_type_stats($type) {
  global $EXPORTED_STATS;

  $total = array();
  $type_stats = array();

  for ($i = 0; $i < xcache_count($type); $i++) {
    $type_stats[] = xcache_info($type, $i);
  }

  foreach($type_stats as $unused => $type_stat) {
    foreach($type_stat as $k => $v) {
      if(!in_array($k, $EXPORTED_STATS)) {
        continue;
      }

      if(!isset($total[$k])) {
        $total[$k] = 0;
      }
      $total[$k] += $v;
    }
  }
  return $total;
}

$code_cache_stats = xcache_type_stats(XC_TYPE_PHP);
$var_cache_stats = xcache_type_stats(XC_TYPE_VAR);

/*
print var_dump($code_cache_stats);
print var_dump($var_cache_stats);
*/

foreach($code_cache_stats as $k => $v) {
  print "xcache.stat{type=code,what=$k}: $v\n";
}
foreach($var_cache_stats as $k => $v) {
  print "xcache.stat{type=var,what=$k}: $v\n";
}
print "xcache.pid: " . getmypid();
?>
